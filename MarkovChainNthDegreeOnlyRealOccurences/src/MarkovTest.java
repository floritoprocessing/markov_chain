import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import net.florito.markov.MarkovChain;


public class MarkovTest {

	// load bible
	// strip 1Sa 2:6 (word - space - word - space)

	public static void main(String[] args) {
		try {
			new MarkovTest();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private static String allowed = "abcdefghijklmnopqrstuvwxyz., \n"; //0123456789
	private static int[] allowedIDs = stringToInts(allowed);
	
	private String rootPath = "D:\\Dropbox\\Shared Research\\java\\MarkovChain source texts\\";
	
	public MarkovTest() throws IOException, ClassNotFoundException {
		
		boolean generateAndSave = false;
		MarkovChain markov;
		
		if (generateAndSave) {
			// loading bible
			String name = "bbe.txt";
			//String name = "aeropagiatica pg608.txt";
			//String name = "schillerbio pg14997.txt";
			//int[] ids = stringToInts("This is a test text.".toLowerCase()); //stringsToInts(loadText(name));
			int[] ids = stringsToInts(loadText(name));
			
			int degree = 3;
			String output = name+".l"+degree+".markov";
			
			int max = 200000;
			if (ids.length>max) {
				int[] newids = new int[max];
				System.arraycopy(ids, 0, newids, 0, max);
				ids = newids;
			}
			
			
			File outputFile = new File(rootPath+output);
			markov = new MarkovChain(allowedIDs, degree);
			ids = markov.legalize(ids);
			markov.createRelationships(ids);
			MarkovChain.save(outputFile, markov);
		
		}
		
		else {
			
			File inputFile = new File(rootPath+"bbe_20000.txt.l4.markov");//bbe_20000.txt.l3.markov");//"bbe.txt.l1.markov");//bbe_50000.txt.l4.markov");
			markov = MarkovChain.load(inputFile);
			
		}
		
		
		System.out.println(markov.relationshipsToString());
		
		System.out.println();
		int[] outids = markov.createChain(5000);
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<outids.length;i++) {
			sb.append((char)outids[i]);
			if ((char)outids[i]=='.') {
				sb.append("\n");
			}
		}
		System.out.println(sb.toString());
	}

	

	/**
	 * Loads bible, removes reference to chapter and makes it lower case
	 * @return
	 */
	private String[] loadText(String name) {
		
		ArrayList<String> lines = new ArrayList<String>();
		
		try {
			File file = new File(rootPath+name);
			System.out.println("Loading "+file);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int count=0;
			while ((line=br.readLine())!=null) {
				
				int firstSpace = line.indexOf(" ");
				int secondSpace = line.indexOf(" ", firstSpace+1);
				line = line.substring(secondSpace+1);
				
				
				lines.add(line.toLowerCase()+"\n");
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return lines.toArray(new String[0]);
		
		
	}
	
	private static int[] stringToInts(String string) {
		int[] out = new int[string.length()];
		for (int i=0;i<out.length;i++) {
			out[i] = (int)string.charAt(i);
		}
		return out;
	}
	
	private static int[] stringsToInts(String[] strings) {
		ArrayList<Integer> bibleIntegers = new ArrayList<>();
		for (int i=0;i<strings.length;i++) {
			int[] lineIds = stringToInts(strings[i]);
			for (int j=0;j<lineIds.length;j++) {
				bibleIntegers.add(lineIds[j]);
			}
		}
		int[] bibleIDs = new int[bibleIntegers.size()];
		for (int i=0;i<bibleIDs.length;i++) {
			bibleIDs[i] = bibleIntegers.get(i);
		}
		return bibleIDs;
	}
	
}
