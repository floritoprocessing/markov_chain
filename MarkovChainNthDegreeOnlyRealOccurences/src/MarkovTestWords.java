import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

import net.florito.markov.MarkovChain;


public class MarkovTestWords {

	// load bible
	// strip 1Sa 2:6 (word - space - word - space)

	public static void main(String[] args) {
		try {
			new MarkovTestWords();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	//private static String allowed = "abcdefghijklmnopqrstuvwxyz., \n"; //0123456789
	//private static int[] allowedIDs = stringToInts(allowed);
	
//	private final String rootPath = "D:\\Dropbox\\Shared Research\\java\\MarkovChain source texts\\";
	
	private final String markovSourceText = "bbe.txt";
	private final int degree = 3;
//	private final String markovFileName = markovSourceText+"_words.l"+degree+".markov";
//	private final File markovFile = new File(rootPath+markovFileName);
	
	class WordCollection {
		ArrayList<String> uniqueWords;
		String[] allWords;
	}
	
	/**
	 * Loads the supplied text if the Markov file does not exist, otherwise creates and saves the Markov file
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public MarkovTestWords() throws IOException, ClassNotFoundException {
		
		MarkovChain markov;
		
		
		WordCollection collection = createWordCollection();
			
		
		File markovFile = null;
		String markovFileName = markovSourceText+"_words.l"+degree+".markov";
		try {
			File binFolder = new File(getClass().getResource("").toURI());
			File srcFolder = new File(binFolder.getParentFile(), "src");
			markovFile = new File(srcFolder, markovFileName);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if (!markovFile.exists() || !markovFile.isFile()) {
			
			System.out.println("Generating "+markovFileName);
			
			int[] allowedIDs = new int[collection.uniqueWords.size()];
			for (int i=0;i<allowedIDs.length;i++) {
				allowedIDs[i] = i;
			}
			
			int[] ids = new int[collection.allWords.length];
			for (int i=0;i<ids.length;i++) {
				ids[i] = collection.uniqueWords.indexOf(collection.allWords[i]);
			}
			
			markov = new MarkovChain(allowedIDs, degree);
			//ids = markov.legalize(ids);
			markov.createRelationships(ids);
			MarkovChain.save(markovFile, markov);
		
		}
		
		else {
			
			System.out.println("Loading "+markovFile.getName());
			//File inputFile = new File(rootPath+"bbe_20000.txt.l4.markov");//bbe_20000.txt.l3.markov");//"bbe.txt.l1.markov");//bbe_50000.txt.l4.markov");
			markov = MarkovChain.load(markovFile); //inputFile
			
		}
		
		
		System.out.println("Creating text...");
		
		int[] outids = markov.createChain(5000);
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<outids.length;i++) {
			sb.append(collection.uniqueWords.get(outids[i]));
			sb.append(" ");
		}
		System.out.println(sb.toString());
		
	}


	private WordCollection createWordCollection() {
		// loading bible
		//String name = "aeropagiatica pg608.txt";
		//String name = "schillerbio pg14997.txt";
		//int[] ids = stringToInts("This is a test text.".toLowerCase()); //stringsToInts(loadText(name));
		
		WordCollection collection = new WordCollection();
		
		String[] lines = loadText(markovSourceText, true);
		float biblePerc = 0.1f;
		if (biblePerc<1) {
			int newCount = (int)(lines.length*biblePerc);
			String[] newLines = new String[newCount];
			System.arraycopy(lines, 0, newLines, 0, newCount);
			lines = newLines;
		}
		
		System.out.println("Formatting text");
		lines = removeCharacters(lines, "[\\(\\)0123456789]");
		collection.allWords = createSeparatedDotsCommasAndReturns(lines);
		collection.uniqueWords = new ArrayList<>();
		
		System.out.print("Compiling unique words...");
		int count=0;
		float lastPerc=-1000;
		for (String word:collection.allWords) {
			if (!collection.uniqueWords.contains(word)) {
				collection.uniqueWords.add(word);
			}
			if (count%1000==0) {
				float perc = 100f*(float)count/(float)collection.allWords.length;
				if (Math.abs(perc-lastPerc)>=5) {
					System.out.print((int)perc+"..");
					lastPerc = perc;
				}
			}
			count++;
			
		}
		System.out.println();
		System.out.printf("Compiled %1$1s words into %2$1s unique words.%n", 
				collection.allWords.length, 
				collection.uniqueWords.size());
		return collection;
	}

	
	private String[] createSeparatedDotsCommasAndReturns(String[] bibleLines) {
		
		ArrayList<String> out = new ArrayList<>();
		for (String bibleLine:bibleLines) {
			
			bibleLine = bibleLine.replaceAll("\\."," . ");
			bibleLine = bibleLine.replaceAll("\\?"," ? ");
			bibleLine = bibleLine.replaceAll(","," , ");
			bibleLine = bibleLine.replaceAll(":"," : ");
			bibleLine = bibleLine.replaceAll(";"," ; ");
			bibleLine = bibleLine.replaceAll("\\("," ( ");
			bibleLine = bibleLine.replaceAll("\\)"," ) ");
			bibleLine = bibleLine.replaceAll("\n"," \n ");
			bibleLine = bibleLine.trim().replaceAll(" +"," ");
			
			String[] bibleWords = bibleLine.split(" ");
			out.addAll(Arrays.asList(bibleWords));
			out.add("\n");
		}
		
		return out.toArray(new String[0]);
	}
	
	private static String[] removeCharacters(String[] input, String regex) {
		String[] out = new String[input.length];
		for (int i=0;i<out.length;i++) {
			String line = input[i].trim().replaceAll(regex, "");
			out[i] = line;
		}
		return out;
	}
	
	private static String[] isolateCharacter(String[] input, String[] chars) {
		String[] out = new String[input.length];
		for (int i=0;i<out.length;i++) {
			String line = input[i];
			for (int j=0;j<chars.length;j++) {
				line = line.replaceAll(chars[j], " "+chars[j]+" ");
				line = line.replaceAll(" +"," ");
			}
			out[i] = line;
			System.out.println(line);
		}
		return out;
	}

	/**
	 * Loads bible, removes reference to chapter and makes it lower case
	 * @return
	 */
	private String[] loadText(String name, boolean isBible) {
		
		ArrayList<String> lines = new ArrayList<String>();
		
		BufferedReader br = null;
		try {
			
			File file = null;
			try {
				File binFolder = new File(getClass().getResource("").toURI());
				File srcFolder = new File(binFolder.getParentFile(), "src");
				file = new File(srcFolder, name);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			System.out.println("Loading text "+file)
			
//			File file = new File(rootPath+name);
			System.out.println("Loading "+file);
			br = new BufferedReader(new FileReader(file));
			String line;
			//int count=0;
			while ((line=br.readLine())!=null) {
				
				if (isBible) {
					int firstSpace = line.indexOf(" ");
					int secondSpace = line.indexOf(" ", firstSpace+1);
					line = line.substring(secondSpace+1);
				}
				
				lines.add(line.toLowerCase()+"\n");
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return lines.toArray(new String[0]);
		
		
	}
	
	private static int[] stringToInts(String string) {
		int[] out = new int[string.length()];
		for (int i=0;i<out.length;i++) {
			out[i] = (int)string.charAt(i);
		}
		return out;
	}
	
	private static int[] stringsToInts(String[] strings) {
		ArrayList<Integer> bibleIntegers = new ArrayList<>();
		for (int i=0;i<strings.length;i++) {
			int[] lineIds = stringToInts(strings[i]);
			for (int j=0;j<lineIds.length;j++) {
				bibleIntegers.add(lineIds[j]);
			}
		}
		int[] bibleIDs = new int[bibleIntegers.size()];
		for (int i=0;i<bibleIDs.length;i++) {
			bibleIDs[i] = bibleIntegers.get(i);
		}
		return bibleIDs;
	}
	
}
