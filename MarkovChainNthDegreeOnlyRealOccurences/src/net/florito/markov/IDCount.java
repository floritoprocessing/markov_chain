package net.florito.markov;

import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;

public class IDCount implements Serializable {

	private static final long serialVersionUID = 4319766291426424963L;

	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeInt(id);
		out.writeInt(count);
	}
	
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		id = in.readInt();
		count = in.readInt();
	}
	
	private void readObjectNoData() throws ObjectStreamException {
		throw new ObjectStreamException() {
			private static final long serialVersionUID = 381019938741799353L;
		};
	}
	
	public int id;
	public int count;
	
	public IDCount(int id, int count) {
		this.id = id;
		this.count = count;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IDCount other = (IDCount) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
}
