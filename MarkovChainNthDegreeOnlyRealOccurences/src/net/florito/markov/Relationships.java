package net.florito.markov;

import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Relationships implements Serializable {

	private static final long serialVersionUID = -6430614022999581589L;

	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeInt(idCombinations.size());
		for (IDCombination combo:idCombinations) 
			out.writeObject(combo);
	}
	
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		idCombinations = new ArrayList<>();
		int count = in.readInt();
		for (int i=0;i<count;i++) {
			idCombinations.add((IDCombination)in.readObject());
		}
	}
	
	private void readObjectNoData() throws ObjectStreamException {
		throw new ObjectStreamException() {
			private static final long serialVersionUID = 5982026084638614903L;
		};
	}
	
	private ArrayList<IDCombination> idCombinations = new ArrayList<>();
	
	public void addCount(int[] srcIDs, int dstID) {
		
		IDCombination idCombination = new IDCombination(srcIDs, dstID, 1);
		
		int combinationIndex = idCombinations.indexOf(idCombination);
		
		// know src combo: i.e. 'dem','o'
		
		if (combinationIndex>=0) {
			idCombinations.get(combinationIndex).count++;
		}
		else {
			idCombinations.add(idCombination);
		}
		
	}
	
	
	/**
	 * Returns null if no combo found
	 * @param srcIDs
	 * @param random
	 * @return
	 */
	public Integer getDstIdFor(int[] srcIDs, double random) {
		
		// get al combos with these srcIDs
		ArrayList<IDCombination> selectedSrcIdCombinations = new ArrayList<>();
		for (IDCombination idCombination:idCombinations) {
			if (Arrays.equals(srcIDs, idCombination.srcIDs)) {
				selectedSrcIdCombinations.add(idCombination);
			}
		}
		
		if (selectedSrcIdCombinations.size()==0) {
			return null;
		}
		
		else {
			
			long totalCount = 0;
			for (IDCombination idCombination:selectedSrcIdCombinations) {
				totalCount += idCombination.count;
			}
			
			long stack = 0;
			for (IDCombination idCombination:selectedSrcIdCombinations) {
				stack += idCombination.count;
				double stackedPercentage = (double)stack / (double)totalCount;
				if (random<stackedPercentage) {
					return idCombination.dstID;
				}
			}
			
			return null;
			
			
		}
		
	}
	
	public IDCombination get(int index) {
		return idCombinations.get(index);
	}
	
	public int size() {
		return idCombinations.size();
	}

}
