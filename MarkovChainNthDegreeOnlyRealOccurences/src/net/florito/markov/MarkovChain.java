package net.florito.markov;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.text.Format;
import java.util.ArrayList;
import java.util.Formatter;

public class MarkovChain implements Serializable {

	public static void save(File file, MarkovChain markov) throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
		oos.writeObject(markov);
		oos.close();
	}
	
	public static MarkovChain load(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
		MarkovChain out = (MarkovChain)ois.readObject();
		ois.close();
		return out;
	}
	
	
	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeInt(uniqueIDs);
		out.writeInt(allowedIDs.length);
		for (int i=0;i<allowedIDs.length;i++) 
			out.writeInt(allowedIDs[i]);
		out.writeInt(maxDegree);
		out.writeInt(occurences.size());
		for (IDCount idCount:occurences) {
			out.writeObject(idCount);
		}
		out.writeInt(relationshipCollection.length);
		for (Relationships relationships:relationshipCollection) {
			out.writeObject(relationships);
		}
	}
	
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		uniqueIDs = in.readInt();
		allowedIDs = new int[in.readInt()];
		for (int i=0;i<allowedIDs.length;i++) 
			allowedIDs[i] = in.readInt();
		maxDegree = in.readInt();
		int count = in.readInt();
		occurences = new ArrayList<>();
		for (int i=0;i<count;i++) 
			occurences.add((IDCount)in.readObject());
		relationshipCollection = new Relationships[in.readInt()];
		for (int i=0;i<relationshipCollection.length;i++)
			relationshipCollection[i] = (Relationships)in.readObject();
	}
	
	private void readObjectNoData() throws ObjectStreamException {
		throw new ObjectStreamException() {
			private static final long serialVersionUID = -6526728889247029339L;
		};
	}
		 
	// ALL IS FINAL!
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8494998997374114314L;
	private int uniqueIDs;
	private int[] allowedIDs;
	private int maxDegree;
	private ArrayList<IDCount> occurences = new ArrayList<>();
	/**
	 * Relationships indexed by degree. [0] = 1st degree, [1] = 2nd degree and so on
	 */
	private Relationships[] relationshipCollection;
	
	
	
	public MarkovChain(int[] allowedIDs, int degree) {
		
		this.uniqueIDs = allowedIDs.length;
		
		this.allowedIDs = new int[allowedIDs.length];
		System.arraycopy(allowedIDs, 0, this.allowedIDs, 0, allowedIDs.length);
		
		this.maxDegree = Math.max(1,degree);
		
		relationshipCollection = new Relationships[maxDegree];
	}

	
	public boolean isLegal(int id) {
		for (int i=0;i<uniqueIDs;i++) {
			if (id==allowedIDs[i]) {
				return true;
			}
		}
		return false;
	}
	
	
	public int[] legalize(int[] IDs) {
		
		ArrayList<Integer> out = new ArrayList<>();
		for (int id:IDs) {
			if (isLegal(id)) {
				out.add(id);
			}
		}
		Integer[] outArr = out.toArray(new Integer[0]);
		int[] o = new int[out.size()];
		for (int i=0;i<o.length;i++) {
			o[i] = outArr[i];
		}
		
		return o;
		
	}


	public void createRelationships(int[] IDs) {
		
		for (int id:IDs) {
			IDCount idCount = new IDCount(id, 1);
			int foundIndex = occurences.indexOf(idCount); // equality based only on id
			if (foundIndex>=0) {
				occurences.get(foundIndex).count++;
			} else {
				occurences.add(idCount);
			}
		}
		/*for (IDCount idCount:occurences) {
			System.out.println((char)idCount.id+": "+idCount.count);
		}*/
		
		for (int degree=1;degree<=maxDegree;degree++) {
		
			// create nth order relationships
			System.out.println("Create "+degree+". order relationships");
			
			Relationships relationships = new Relationships();
			relationshipCollection[degree-1] = relationships;
			
			// go through all IDs
			float lastPerc = -1000;
			for (int i=0;i<IDs.length-degree;i++) {
			
				int[] srcIDs = createSrcIDs(IDs, i, degree);
				int dstID = IDs[i+degree];
				//System.out.println(intsToChars(srcIDs)+" -> "+(char)dstID);
				
				relationships.addCount(srcIDs,dstID);
				
				if (i%1000==0) {
					float perc = (float)i/(float)(IDs.length-degree-1);
					if (Math.abs(perc-lastPerc)>=0.05) {
						System.out.print((int)(100*perc)+"..");
						lastPerc = perc;
					}
				}
				else if (i==IDs.length-degree-1) {
					System.out.println();
				}
			}
			
			System.out.println("With a list of "+IDs.length+", "+relationships.size()+" unique combinations found");
		}
	}


	private static int[] createSrcIDs(int[] IDs, int idIndex, int degree) {
		
		int[] out = new int[degree];
		for (int i=0;i<degree;i++) {
			out[i] = IDs[idIndex+i];
		}
		
		return out;
	}


	public Integer createRandomLetter() {
		// create first letter
		float random = (float)(Math.random());
		long totalCount = 0;
		for (IDCount idCount:occurences) {
			totalCount += idCount.count;
		}
		Integer randomLetter = null;
		long stack = 0;
		occ: for (IDCount idCount:occurences) {
			stack += idCount.count;
			double percentageStacked = (double)stack / (double)totalCount;
			if (random<percentageStacked) {
				randomLetter = idCount.id;
				break occ;
			}
		}
		
		if (randomLetter==null) {
			throw new RuntimeException("whoops, first letter zero");
		}
		return randomLetter;
	}
	
	public int[] createChain(int amount) {
		
		int[] out = new int[amount];
		out[0] = createRandomLetter();
		
		// generate all other letters
		for (int letterOutIndex=1;letterOutIndex<amount;letterOutIndex++) {
			
			int degree = Math.min(letterOutIndex, maxDegree);
			int[] srcIDs = createSrcIDs(out, letterOutIndex-degree, degree);
			//System.out.print(intsToChars(srcIDs)+"..");
			
			Relationships relationship = relationshipCollection[degree-1];
			Integer nextID = relationship.getDstIdFor(srcIDs, Math.random());
			if (nextID==null) {
				nextID = createRandomLetter();
				//System.out.println("generated random letter");
			} else {
				//System.out.println("found relationship");
			}
			out[letterOutIndex] = nextID;
			
		}
		
		return out;
	}

	
	
	public static String intsToChars(int[] ids) {
		String out = "";
		for (int id:ids) {
			char c = (char)id;
			if (c=='\n') {
				out += "[NL]";
			} else if (c==' ') {
				out += "' '";
			} else{
				out += (char)id;
			}
		}
		return out;
	}

	public String relationshipsToString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MaxDegree="+maxDegree+"\n");
		for (int degree=1;degree<=maxDegree;degree++) {
			sb.append("Degree="+degree+"\n");
			
			Relationships relationships = relationshipCollection[degree-1];
			
			long totalCount = 0;
			for (int j=0;j<relationships.size();j++) {
				
				IDCombination idCombination = relationships.get(j);
				totalCount += idCombination.count;
			}
			
			Formatter formatter = new Formatter(sb);
			for (int j=0;j<relationships.size();j++) {
				IDCombination idCombination = relationships.get(j);
				double percentage = (double)idCombination.count / (double)totalCount;
				
				String dst = ""+(char)idCombination.dstID;
				if (dst=="\n" || dst=="\r") dst="[NL]";
				else if (dst==" ") dst="' '";
				formatter.format("%.4f: %2$1s -> %3$1s %n", 
						(percentage*100),
						intsToChars(idCombination.srcIDs), 
						dst);
			}
			
			
		}
		return sb.toString();
	}
}
