package net.florito.markov;

import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.Arrays;


public class IDCombination implements Serializable {

	private static final long serialVersionUID = -7795237878660772796L;

	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeInt(srcIDs.length);
		for (int id:srcIDs) 
			out.writeInt(id);
		out.writeInt(dstID);
		out.writeInt(count);
	}
	
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		srcIDs = new int[in.readInt()];
		for (int i=0;i<srcIDs.length;i++)
			srcIDs[i] = in.readInt();
		dstID = in.readInt();
		count = in.readInt();
	}
	
	private void readObjectNoData() throws ObjectStreamException {
		throw new ObjectStreamException() {
			private static final long serialVersionUID = -9023216512201923343L;
		};
	}
	
	public int[] srcIDs;
	public int dstID;
	public int count=0;
	
	public IDCombination(int[] srcIDs, int dstID, int count) {
		this.srcIDs = new int[srcIDs.length];
		System.arraycopy(srcIDs, 0, this.srcIDs, 0, srcIDs.length);
		this.dstID = dstID;
		this.count = count;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dstID;
		result = prime * result + Arrays.hashCode(srcIDs);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IDCombination other = (IDCombination) obj;
		if (dstID != other.dstID)
			return false;
		if (!Arrays.equals(srcIDs, other.srcIDs))
			return false;
		return true;
	}

	

	
	
}
