package testers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

import net.florito.markovchain.MarkovChain;

public class TestMarkovChainLibOpt {

	
	int RESULT_COUNT = 10;
	int CHAIN_LENGTH = 150;
	
	public TestMarkovChainLibOpt() {
		/*//String filename = "bbeNoChaptersUntilRut.txt";
		String filename = "aeropagiatica pg608 no empty lines.txt";
		//String filename = "the great divide.txt";
		File path = new File("D:\\Dropbox\\Shared Research\\java\\MarkovChain source texts");
		File file = new File(path , filename);
		
		String text = loadText(file, true);
		String modText = separateSpecialChars(text).replaceAll(" +"," ");
		String[] words = modText.split(" ");
		
		words = maxWords(words, 60000);*/
		
		
		File bowieFolder = null;
		try {
			File binFolder = new File(getClass().getResource("").toURI());
			bowieFolder = new File(binFolder.getParentFile(), "Bowie lyrics");
//			File folder = binFolder.getParentFile().getParentFile().getParentFile();
			//File sourceTextFolder = 
			System.out.println(bowieFolder+" "+bowieFolder.exists());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		if (true) return;
		
		File[] lyricFiles = bowieFolder.listFiles();
		
		
		Vector<String> wordList = new Vector<>();
		for (File file:lyricFiles) {
			if (file.isFile() && file.getName().toLowerCase().endsWith(".txt")) {
				String text = loadText(file,true);
				text = text.replaceAll(" +", " ");
				text = text.replaceAll("\"", "");
				
				System.out.print("\""+file.getName()+"\" ");
				//System.out.println(text);
				List<String> wordsInSong = Arrays.asList(text.split(" "));
				wordList.addAll(wordsInSong);
				wordList.add(newLine);
				wordList.add(newLine);
			}
		}
		String[] words = wordList.toArray(new String[0]);
		
		MarkovChain.DEBUG = true;
		MarkovChain<String> chain = new MarkovChain<>(2);
		chain.train(words);
		
	
		//System.out.println(chain.getChainObjectAmount()+" different combinations out of "+words.length+" objects.");
		
		
		for (int l=0;l<RESULT_COUNT;l++) {
			System.out.println();
			System.out.println("--------------------------------------------------");
			System.out.println("RESULT: ("+l+"):");
			
			//Vector<String> objects = chain.createChain(newLine,newLine);//".");
			Vector<String> objects = chain.createChain(newLine,CHAIN_LENGTH);//".");
			for (int i=0;i<objects.size();i++) {
				if (objects.get(i).equals(newLine)) {
					System.out.println();
				} else {
					System.out.print(objects.get(i)+" ");
				}
			}
			System.out.println();
			System.out.println("--------------------------------------------------");
			System.out.println();
		}
	}

	private static String[] maxWords(String[] words, int max) {
		if (words.length>max) {
			String[] temp = new String[max];
			System.arraycopy(words, 0, temp, 0, max);
			words = temp;
		}
		return words;
	}

	
	
	
	private static String newLine = "NEWLINE";//System.getProperty("line.separator");
	static private String loadText(File file, boolean newlineIsObject) {
		System.out.print("Loading "+file+"... ");
		String text;
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while ((line=reader.readLine())!=null) {
				sb.append(line.toLowerCase());
				if (newlineIsObject) {
					sb.append(" "+newLine+" ");
				} else {
					sb.append(" ");
				}
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		text = sb.toString();
		System.out.println("done.");
		return text;
	}
	
	
	static char[] specialSigns = new char[] { '(', ')', '.', ':', ';', ',', '?', '!' };
	static boolean isSpecialSign(char c) {
	  for (int i=0;i<specialSigns.length;i++) {
	    if (c==specialSigns[i]) {
	      return true;
	    }
	  }
	  return false;
	}
	
	static private String separateSpecialChars(String text) {
		System.out.print("Separating characters... ");
		
		//Pattern pattern = Pattern.compile("[A-Za-z0-9]");
		Pattern pattern = Pattern.compile("[^\\x0B\\f]");
		//TODO Remove weird chars
		
		StringBuilder sb = new StringBuilder();
		int textLen = text.length();
		char c;
		for (int i=0;i<textLen;i++) {
			c = text.charAt(i);
			if (isSpecialSign(c)) {
				sb.append(" "+c+" ");
			} else if (pattern.matcher(""+c).find()){
				sb.append(c);
			}
		}
		System.out.println("done.");
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		new TestMarkovChainLibOpt();
	}
}
