"Watch That Man"

Shakey threw a party 
that lasted all night
Everybody drank a lot of something nice
There was an old fashioned band 
of married men
Looking up to me 
for encouragement 
- it was so-so

The ladies looked bad 
but the music was sad
No one took their eyes off Lorraine
She shimmered and she strolled like a Chicago moll
Her feathers looked better and better 
- it was so-so

Yea! it was time to unfreeze
When the Reverend Alabaster danced on his knees
Slam! so it wasn't a game
Cracking all the mirrors in shame

Watch that man!3 Oh honey, watch that man
He talks like a jerk but he could eat you with a fork and 
spoon
Watch that man! Oh honey, watch that man
He walks like a jerk
But he's only taking care of the room
Must be in tune

A Benny Goodman 1fan painted holes in his hands
So Shakey hung him up to dry
The pundits3were joking 
the manholes were smoking
And every bottle battled
with the reason why

The girl on the phone wouldn't leave me alone
A throw back from someone's LP
A lemon in a bag played 
the Tiger Rag3 
And the bodies on the screen stopped bleeding

Yeah! I was shaking like a leaf
For I couldn't understand the conversation
Yeah! I ran to the street, looking for information

Watch that man




"Aladdin Sane"

Watching him dash away, 
swinging an old bouquet 
- dead roses
Sake and strange divine Uh-h-h-uh-h-uh you'll make it
Passionate bright young things, 
takes him away to war - 
don't fake it
Saddening glissando strings
Uh-h-h-uh-h-uh - you'll make it

Who'll love Aladdin Sane
Battle cries and champagne just in time for sunrise
Who'll love Aladdin Sane

Motor sensational, Paris or maybe hell - I'm waiting
Clutches of sad remains
Waits for Aladdin Sane - you'll make it

Who'll love Aladdin Sane
Millions weep a fountain, 
just in case of sunrise

Who'll love Aladdin Sane

We'll love Aladdin Sane
Love Aladdin Sane

Who'll love Aladdin Sane
Millions weep a fountain, 
just in case of sunrise

Who'll love Aladdin Sane

We'll love Aladdin Sane
We'll love Aladdin Sane




"Drive-In Saturday"

Let me put my arms 
around your head
Gee, it's hot, let's go to bed
Don't forget to turn on the light
Don't laugh babe, it'll be alright
Pour me out another phone
I'll ring and see
if your friends are home
Perhaps the strange ones in the dome
Can lend us a book we can read up alone

And try to get it on like once before
When people stared in Jagger's eyes
and scored
Like the video films we saw

His name was always Buddy
And he'd shrug and ask to stay
She'd sigh like Twig the Wonder Kid
And turn her face away
She's uncertain if she likes him
But she knows she really loves him
It's a crash course for the ravers
It's a Drive-in Saturday

Jung the foreman prayed at work
That neither hands nor limbs would burst
It's hard enough to keep formation 
amid this fall out saturation

Cursing at the Astronette 8
Who stands in steel 
by his cabinet
He's crashing out with Sylvian
The Bureau Supply 
for ageing men

With snorting head he gazes to the shore
Which once had raised a sea
that raged no more
Like the video films we saw

It's a Drive-in Saturday




"Panic In Detroit"

He looked a lot like Che Guevara, 
drove a diesel van
Kept his gun in quiet seclusion,
such a humble man
The only survivor of the National People's Gang
Panic in Detroit, I asked for an autograph
He wanted to stay home, I wish someone would phone

Panic in Detroit

He laughed at accidental sirens that broke the evening 
gloom
The police had warned of repercussions

They followed none too soon
A trickle of strangers were all that were left alive
Panic in Detroit, I asked for an autograph
He wanted to stay home, I wish someone would phone

Panic in Detroit

Putting on some clothes I made my way to school
And I found my teacher
crouching in his overalls

I screamed and ran to smash my favorite slot machine
And jumped the silent cars that slept at traffic lights

Having scored a trillion dollars, 
made a run back home

Found him slumped across the table. 
A gun and me alone
I ran to the window. Looked for a plane or two
Panic in Detroit. 
He'd left me an autograph
"Let me collect dust." 
I wish someone would phone

Panic in Detroit




"Cracked Actor"

I've come on a few years from my Hollywood Highs
The best of the last, the cleanest star they ever had

I'm stiff on my legend, 
the films that I made
Forget that I'm fifty 
cause you just got paid

Crack, baby, crack, 
show me you're real
Smack, baby, smack, is that all that you feel
Suck, baby, suck,
give me your head
Before you start professing 
that you're knocking me dead

You caught yourself a trick down 
on Sunset and Vine
But since he pinned you baby
you're a porcupine

You sold me illusions for a sack full of cheques
You've made a bad connection 'cause I just want your sex





"Time"

Time - He's waiting in the wings
He speaks of senseless things
His script is you and me boys

Time - He flexes like a whore
Falls wanking to the floor
His trick is you and me, boy

Time - In Quaaludes and red wine
Demanding Billy Dolls
And other friends of mine
Take your time

The sniper in the brain, regurgitating drain
Incestuous and vain, 
and many other last names
I look at my watch it say 9:25 and I think 
"Oh God I'm still alive"

We should be on by now
La, la, la, la, la, la, la, la

You - are not a victim
You - just scream with boredom
You - are not evicting time

Chimes - Goddamn, you're looking old
You'll freeze and catch a cold
'Cause you've left your coat behind
Take your time

Breaking up is hard, but keeping dark is hateful
I had so many dreams, 
I had so many breakthroughs
But you, my love, were kind, but love has left you 
dreamless
The door to dreams was closed. 
Your park was real dreamless
Perhaps you're smiling now, 
smiling through this darkness
But all I had to give was the guilt for dreaming

We should be on by now
La, la, la, la, la, la, la, la

Yeah, time!



"The Prettiest Star"

Cold fire, you've got everything 
but cold fire
You will be my rest and peace child
I moved up to take a place near you

So tired, it's the sky that makes you feel tired
It's a trick to make you see wide
It can all but break your heart in pieces

[CHORUS]
Staying back in your memory
Are the movies in the past
How you moved is all it takes
To sing a song of when I loved
The Prettiest Star

One day though it might 
as well be someday
You and I will rise up all the way
All because of what you are
The Prettiest Star

One day though it might
as well be someday
You and I will rise up all the way
All because of what you are
The Prettiest Star





"Let's Spend The Night Together"

Well, don't you worry 'bout what's been on my mind
I'm in no hurry 
I can take my time
I'm going red 
and my tongue's getting tired
Out of my head and my mouth's getting dry
I'm h-h-h-high

Let's spend the night together
Now I need you more than ever
Let's spend the night together now

I feel so strong
that I can't disguise, oh my
Well, I just can't apologise, no
Don't hang me up but don't let me down
We could have fun just by fooling around, and around 
and around

Oh, You know I'm smiling baby
You need some guiding baby
I'm just deciding baby

This doesn't happen to me every day
No excuses I've got anyway, heh
I'll satisfy your every need
And I'll know you'll satisfy me, oh my-my-my my-my

Let's spend the night together
Now I need you more than ever

Let's spend the night together
They said we were too young
Our kind of love 
was no fun
But our love 
comes from above
Do it!
Let's make love
Hoo!





"The Jean Genie"

A small Jean Genie
snuck off to the city
Strung out on lasers 
and slash back blazers
Ate all your razors 
while pulling the waiters
Talking bout Monroe 
and walking on Snow White 19
New York's a go-go 19
and everything tastes right
Poor little Greenie

The Jean Genie lives on his back
The Jean Genie loves chimney stacks
He's outrageous, he screams and he bawls
Jean Genie let yourself go!

Sits like a man 
but he smiles like a reptile
She loves him, she loves him but 
just for a short while
She'll scratch in the sand, 
won't let go his hand
He says he's a beautician
and sells you nutrition
And keeps all your dead hair 
for making up underwear
Poor little Greenie

He's so simple minded 
he can't drive his module
He bites on the neon and sleeps in the capsule
Loves to be loved, loves to be loved




"Lady Grinning Soul"

She'll come, she'll go.
She'll lay belief on you
Skin sweet with musky oil
The lady from another grinning soul

Cologne she'll wear. Silver and Americard
She'll drive a beetle car
And beat you down at cool Canasta

And when the clothes are strewn
don't be afraid of the room
Touch the fullness of her breast. 
Feel the love of her caress
She will be your living end

She'll come, she'll go.
She'll lay belief on you
But she won't stake her life on you
How can life become 
her point of view

And when the clothes are strewn 
don't be afraid of the room
Touch the fullness of her breast. 
Feel the love of her caress
She will be your living end