package net.florito.markovchain;

import java.util.Comparator;
import java.util.Vector;

public class IDChain {

	private final int[] ids;
	
	public IDChain(Vector<Integer> ids) {
		this.ids = new int[ids.size()];
		for (int i=0;i<this.ids.length;i++) {
			this.ids[i] = ids.get(i);
		}
	}
	
	public int getID(int index) {
		return ids[index];
	}
	
	public int length() {
		return ids.length;
	}
	
	public String arrToString(int[] ids) {
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<ids.length;i++) {
			sb.append(ids[i]);
			if (i<ids.length-1)
				sb.append(",");
		}
		return sb.toString();
	}
	
	public boolean isPrecurserOf(IDChain otherChain) {
		//System.out.print("follow(): Does other "+arrToString(otherChain.ids)+" follow "+arrToString(ids)+"? ");
		int[] otherIds = otherChain.ids;
		int oLength = otherIds.length;
		
		// other = length1 -> false
		if (oLength==1) {
			//System.out.println("No, other is length 1");
			return false;
		}
		// same length: [][][] (current)
		//                [][][] (other)
		else if (oLength == ids.length) {
			for (int oi=0;oi<oLength-1;oi++) {
				int i = oi+1;
				if (ids[i]!=otherIds[oi]) {
					//System.out.println("Nope(c1).");
					return false;
				}
			}
			//System.out.println("Yes, equal lengths");
			return true;
		}
		// other shorter: [][][][][]   (this)
		//                      [][][] (other)
		else if (ids.length > oLength) {
			for (int oi=0;oi<oLength-1;oi++) {
				int i = oi + (ids.length-oLength) + 1;
				if (ids[i]!=otherIds[oi]) {
					//System.out.println("Nope(c2).");
					return false;
				}
			}
			//System.out.println("Yes, c2");
			return true;
		}
		// other longer: [][][]
		//               [][][][] (exactly 1 longer)
		else if (ids.length == oLength-1) {
			for (int oi=0;oi<oLength-1;oi++) {
				int i = oi;
				if (ids[i]!=otherIds[oi]) {
					//System.out.println("Nope(c3).");
					return false;
				}
			}
			//System.out.println("Yes, c3");
			return true;
		}
		// otherwise nope
		else {
			//System.out.println("MOPE");
			return false;
		}
	}

	
	public static Comparator<IDChain> getChainLengthComparator() {
		return new Comparator<IDChain>() {
			public int compare(IDChain o1, IDChain o2) {
				int l1 = o1.length();
				int l2 = o2.length();
				if (l1<l2) {
					return -1;
				} else if (l1>l2) {
					return 1;
				} else {
					return 0;
				}
			}
		};
	}
	
}
