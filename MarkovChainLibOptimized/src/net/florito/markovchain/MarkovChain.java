package net.florito.markovchain;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class MarkovChain<E> {

	public static boolean DEBUG = false;
	public static boolean DEBUG_TRAINING_DETAILED = false;
	public static boolean DEBUG_CREATING = false;
	
	private static final Comparator<IDChain> compareByChainLength = 
			IDChain.getChainLengthComparator();
	
	private int degree;
	private EventRegistry<E> eventRegistry = new EventRegistry<>();
	
	private boolean idChainRegistrySorted = false;
	private IDChainRegistry idChainRegistry = new IDChainRegistry();
	
	private boolean singleChainAmountCalculated = false;
	private int singleChainAmount = -1;
	
	
	public MarkovChain(int degree) {
		this.degree = Math.max(1, degree);
	}

	
	public void train(E[] events) {
		if (DEBUG)
			System.out.println("training...");
		
		
		eventRegistry.clear();
		idChainRegistry.clear();
		idChainRegistrySorted = false;
		singleChainAmount = -1;
		singleChainAmountCalculated = false;
		
		
		float lastP = -1000;
		for (int i=0;i<events.length;i++) {
			
			float p = (float)i/(float)(events.length-1);
			if (Math.abs(lastP-p)>=0.05) {
				if (DEBUG) 
					System.out.print((int)(p*100)+"..");
				lastP = p;
			}
			if (i==events.length-1) {
				if (DEBUG)
					System.out.println(100);
			}
			
			E newEvent = events[i];
			if (DEBUG_TRAINING_DETAILED)
				System.out.println(newEvent);
			
			// register the event
			eventRegistry.registerEvent(newEvent);
			
			// create all possible chains from this and previous events..
			Vector<Integer> idList= new Vector<>();
			int maxDegree = Math.min(i+1,degree+1);
			for (int iOff=-maxDegree+1;iOff<=0;iOff++) {
				int index = i+iOff;
				
				E event = events[index];
				int eventID = eventRegistry.getIDOfEvent(event);
				idList.add(eventID);
				
				// .. and add them to the idChainRegistry
				IDChain idChain = new IDChain(idList);
				idChainRegistry.add(idChain);
				
				if (DEBUG_TRAINING_DETAILED) {
					E[] str = eventRegistry.getNative(idChain);
					System.out.println("-> "+genericArrayToString(str));
				}
			}
			
		}
		
		
		if (DEBUG) {
			System.out.printf("%d unique idChains.%n",idChainRegistry.size());
		}
		
		
		calculateSingleChainAmount();
	}
	
	
	
	
	private void sortIdChainRegistry() {
		// sort by chain length
		Collections.sort(idChainRegistry, compareByChainLength);
		idChainRegistrySorted = true;
	}
	
	
	
	private void calculateSingleChainAmount() {
		if (!idChainRegistrySorted) {
			sortIdChainRegistry();
		}
		
		// calculate singleChaimAmount
		for (int i=0;i<idChainRegistry.size();i++) {
			if (idChainRegistry.get(i).length()==1) {
				singleChainAmount = i+1;
			} else {
				break;
			}
		}
		singleChainAmountCalculated = true;
	}
	
	
	
	
	private E idToEvent(int id) {
		return eventRegistry.getEvent(id);
	}
	
	private int eventToId(E event) {
		return eventRegistry.getIDOfEvent(event);
	}
	
	private E[] idChainToEvents(IDChain idChain) {
		return eventRegistry.getNative(idChain);
	}
	
	
	
	
	
	
	enum EndTestType {
		END_EVENT, EVENT_COUNT;
	}
	
	class EndTest {
		
		EndTestType type;
		
		//E endEvent;
		int endEventID;
		public EndTest(E endEvent) {
			type = EndTestType.END_EVENT;
			this.endEventID = eventToId(endEvent);
			//this.endEvent = endEvent;
		}
		
		int eventCount;
		public EndTest(int eventCount) {
			type = EndTestType.EVENT_COUNT;
			this.eventCount = eventCount;
		}
		
		
	}
	
	
	public Vector<E> createChain(E startEvent, E endEvent) {
		
		if (startEvent==null) {
			throw new RuntimeException("Start event is null!");
		}
		
		// startEvent registered?
		if (eventToId(startEvent)==-1) {
			throw new RuntimeException("The start event "+startEvent.toString()+" has never been trained!");
		}
	
		if (endEvent==null) {
			throw new RuntimeException("End event is null!");
		}
		
		// endEvent registered?
		int endEventId = eventToId(endEvent);
		if (endEventId==-1) {
			throw new RuntimeException("The end event "+endEvent.toString()+" has never been trained!");
		}
		
		EndTest endTest = new EndTest(endEvent);
		
		return createChain(startEvent, endTest);
		
	}
	
	
	
	public Vector<E> createChain(E startEvent, int count) {
		
		if (startEvent==null) {
			throw new RuntimeException("Start event is null!");
		}
		
		// startEvent registered?
		if (eventToId(startEvent)==-1) {
			throw new RuntimeException("The start event "+startEvent.toString()+" has never been trained!");
		}
	
		EndTest endTest = new EndTest(count);
		
		return createChain(startEvent, endTest);
		
	}


	
	private Vector<E> createChain(E startEvent, EndTest endTest) {
		
		
		
		// single chaimAmount calculation (for random single E)
		if (!singleChainAmountCalculated) {
			calculateSingleChainAmount();
		}
		
		
		
		
		// create output vector of IDs
		
		Vector<Integer> outIDs = new Vector<>();
		
		
		// first event ID based on startEvent
		int startEventID = eventToId(startEvent);
		Vector<IDChain> firstChains = idChainRegistry.getChainsStartingWith(startEventID,2);
		int firstID = -1;
		if (firstChains.size()>0) {
			IDChain randomChain = firstChains.get((int)(firstChains.size()*Math.random()));
			if (randomChain.length()!=2) {
				throw new RuntimeException("Something went wrong. " +
						"I expected chain of length 2, but I got a length "+randomChain.length()+".");
			}
			firstID = randomChain.getID(1);
		} else {
			IDChain randomChain = idChainRegistry.get((int)(singleChainAmount*Math.random()));
			firstID = randomChain.getID(randomChain.length()-1);
		}
		outIDs.add(firstID);
		
		
		
		
		// LOOP
		
		boolean finished = false;
		
		do {
		
			// create an idChain that represents the longest possible current last chain
			int currentOutIndex = outIDs.size()-1;
			int maxChainElements = Math.min(outIDs.size(),degree+1);
			int lowestOutIndex = currentOutIndex-maxChainElements+1;
			Vector<Integer> ids = new Vector<>();
			for (int outIndex=lowestOutIndex;outIndex<=currentOutIndex;outIndex++) {
				ids.add(outIDs.get(outIndex));
			}
			IDChain currentIDChain = new IDChain(ids);
			if (DEBUG_CREATING) {
				System.out.println("["+lowestOutIndex+".."+currentOutIndex+"]: current idChain: "+
						genericArrayToString(
								idChainToEvents(currentIDChain)));
			}
			
			
			// Find all idChains that start with the previous idChain
			Vector<IDChain> possibleFollowUps = new Vector<>();
			for (IDChain idChain:idChainRegistry) {
				if (currentIDChain.isPrecurserOf(idChain)) {
					possibleFollowUps.add(idChain);
				}
			}
			
			
			// no follow ups -> generate random
			int newID = -1;
			if (possibleFollowUps.size()==0) {
				IDChain randomSingleChain = idChainRegistry.get((int)(singleChainAmount*Math.random()));
				if (randomSingleChain.length()!=1) {
					throw new RuntimeException("Something went wrong. " +
							"I expected IDChain length 1, but it was "+randomSingleChain.length());
				}
				newID = randomSingleChain.getID(0);
				if (DEBUG_CREATING) {
					E event = idToEvent(newID);
					System.out.println("No follow-ups found, Choosing random: "+genericToString(event));
				}
			}
			
			
			// we have follow ups!
			else {
				
				if (DEBUG_CREATING) {
					System.out.println(possibleFollowUps.size()+" possible follow-ups found.");
				}
				// sublist from longest chainLength...
				Collections.sort(possibleFollowUps, compareByChainLength);
				
				int longestChainLength = possibleFollowUps.get(possibleFollowUps.size()-1).length();
				int i0=0, i1=possibleFollowUps.size();
				for (int i=0;i<possibleFollowUps.size();i++) {
					if (possibleFollowUps.get(i).length()==longestChainLength) {
						i0 = i;
						break;
					}
				}
				
				if (DEBUG_CREATING) {
					int count = i1-i0;
					System.out.print(count+" follow-ups of length "+longestChainLength+", ");
				}
				
				int randomIndex = random(i0,i1);
				IDChain randomChain = possibleFollowUps.get(randomIndex);
				newID = randomChain.getID(randomChain.length()-1);
				
				if (DEBUG_CREATING) {
					System.out.println("chosen follow-up: "+genericArrayToString(idChainToEvents(randomChain)));
				}
				
			}
			outIDs.add(newID);
		
			
			switch (endTest.type) {
			case END_EVENT:
				finished = (newID==endTest.endEventID);
				break;
			case EVENT_COUNT:
				finished = (outIDs.size()==endTest.eventCount);
				break;
			default:
				finished = true;
				break;
			}
		
			
			//finished = (newID==endEventId);
			
			
		} while (!finished);
		
		
		
		// turn IDs into generic events
		
		Vector<E> out = new Vector<>();
		for (Integer outID:outIDs) {
			out.add(eventRegistry.getEvent(outID));
		}
		return out;
		
	}
	
	
	
	
	
	
	
	
	
	private static int random(int i0, int i1) {
		int di = i1-i0;
		return (int)(i0 + Math.random()*di);
	}

	private String genericToString(E gen) {
		StringBuilder sb = new StringBuilder();
		sb.append(gen.toString());
		sb.append("(");
		sb.append(eventRegistry.getIDOfEvent(gen));
		sb.append(")");
		return sb.toString();
	}
	
	private String genericArrayToString(E[] str) {
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<str.length;i++) {
			sb.append(str[i].toString());
			sb.append("(");
			sb.append(eventRegistry.getIDOfEvent(str[i]));
			sb.append(")");
			if (i<str.length-1) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}

	
}
