package net.florito.markovchain;

import java.util.Vector;

/**
 * Registers all possible markov chain events
 * @author Marcus
 *
 * @param <T>
 */
public class EventRegistry<T> {
	
	private Vector<T> uniqueEvents = new Vector<>();
	
	public EventRegistry() {
	}
	

	public void clear() {
		uniqueEvents.clear();
	}
	
	/**
	 * Registers the event if it didn't exist yet
	 * @param event
	 */
	public void registerEvent(T event) {
		if (!uniqueEvents.contains(event)) {
			uniqueEvents.add(event);
		}
	}
	
	/**
	 * Returns the event of a given id
	 * @param id
	 * @return
	 */
	public T getEvent(int id) {
		return uniqueEvents.get(id);
	}
	
	/**
	 * Returns the id of the event
	 * @param event
	 * @return
	 */
	public int getIDOfEvent(T event) {
		return uniqueEvents.indexOf(event);
	}

	/**
	 * Converts an eventChain of indices to an array of native objects
	 * @param idChain
	 * @return
	 */
	public T[] getNative(IDChain idChain) {
		@SuppressWarnings("unchecked")
		T[] out = (T[])new Object[idChain.length()];
		for (int i=0;i<out.length;i++) {
			int index = idChain.getID(i);
			out[i] = uniqueEvents.get(index);
		}
		return out;
	}

	

}
