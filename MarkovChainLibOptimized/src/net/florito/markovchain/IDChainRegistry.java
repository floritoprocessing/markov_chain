package net.florito.markovchain;

import java.util.Vector;

public class IDChainRegistry extends Vector<IDChain> {

	private static final long serialVersionUID = 2214307914284582772L;

	public IDChainRegistry() {
		super();
	}

	public Vector<IDChain> getChainsStartingWith(int eventID, int chainLength) {
		Vector<IDChain> out = new Vector<>();
		
		for (IDChain idChain:this) {
			if (idChain.length()==chainLength && idChain.getID(0)==eventID) {
				out.add(idChain);
			}
		}
		
		return out;
	}
	
	
}
