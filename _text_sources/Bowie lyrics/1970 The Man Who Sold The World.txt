"Width Of A Circle"

In the corner of the morning in the past
I would sit and blame the master first and last
All the roads were straight and narrow
And the prayers were small and yellow
And the rumour spread that I was aging fast
Then I ran across a monster who was sleeping by a tree.

And I looked and frowned and the monster was me

Well, I said hello and I said hello
And I asked "Why not?" and I replied "I don't know"
So we asked a simple black bird, who was happy as can be

And he laughed insane and quipped "KAHLIL GIBRAN"
So I cried for all the others till the day was nearly through
For I realized that God's a young man too

So I said "So long" and I waved "Bye-bye"
And I smashed my soul and traded my mind

Got laid by a young bordello
I was vaguely half asleep
For which my reputation swept back home in drag

And the moral of this magic spell
Negotiates my hide
When God did take my logic for a ride
(Riding along)

He swallowed his pride and puckered his lips
And showed me the leather belt round his hips
My knees were shaking my cheeks aflame
He said "You'll never go down to the Gods again"
(Turn around,go back!)

He struck the ground a cavern appeared
And I smelt the burning pit of fear
We crashed a thousand yards below
I said "Do it again, do it again"
(Turn around,go back!)

His nebulous body swayed above
His tongue swollen with devil's love
The snake and I, a venom high
I said "Do it again, do it again"
(Turn around, go back!)

Breathe, breathe, breathe deeply

And I was seething, breathing deeply
Spitting sentry, horned and tailed

Waiting for you




"All The Madmen"

Day after day 
They send my friends away 
To mansions cold and grey 
To the far side of town 
Where the thin men stalk the streets 
While the sane stay underground 

Day after day 
They tell me I can go 
They tell me I can blow 
To the far side of town 
Where it's pointless to be high 
'Cause it's such a long way down 
So I tell them that 
I can fly, I will scream, I will break my arm 

I will do me harm 
Here I stand, foot in hand, talking to my wall 
I'm not quite right at all...am I? 

Don't set me free, I'm as heavy as can be 
Just my librium and me 
And my E.S.T. makes three 

'Cause I'd rather stay here 
With all the madmen 
Than perish with the sadmen roaming free 

And I'd rather play here 
With all the madmen 
For I'm quite content they're all as sane as me 

(Where can the horizon lie 
When a nation hides 
Its organic minds in a cellar...dark and grim
They must be very dim) 

Day after day 
They take some brain away 
Then turn my face around 
To the far side of town 
And tell me that it's real 
Then ask me how I feel 

Here I stand, foot in hand, talking to my wall 
I'm not quite right at all 
Don't set me free, I'm as helpless as can be 
My libido's split on me 
Gimme some good 'ole lobotomy 

'Cause I'd rather stay here 
With all the madmen 
Than perish with the sadmen 
Roaming free And I'd rather play here 
With all the madmen 
For I'm quite content 
They're all as sane as me 
Zane, Zane, Zane Ouvre le Chien





"Black Country Rock"

Pack a pack horse up and rest up here on
Black Country Rock

You never know, you might find it here on
Black Country Rock

Some say the view is crazy
But you may adopt another point of view
So if it's much too hazy
You can leave my friend and me with fond adieu




"After All"

Please trip them gently, they don't like to fall, Oh by jingo

There's no room for anger, we're all very small, Oh by jingo

We're painting our faces and dressing in thoughts from the skies, 
From paradise 
But they think that we're holding a secretive ball. 
Won't someone invite them 
They're just taller children, that's all, after all 

Man is an obstacle, sad as the clown, Oh by jingo 
So hold on to nothing, and he won't let you down, Oh by jingo 
Some people are marching together and some on their own
Quite alone 
Others are running, the smaller ones crawl
But some sit in silence, they're just older children 
That's all, after all 

I sing with impertinence, shading impermanent chords, with my words 
I've borrowed your time and I'm sorry I called 
But the thought just occurred that we're nobody's children At all, after all 
Live till your rebirth and do what you will, Oh by jingo 
Forget all I've said, please bear me no ill, Oh by jingo 

After all, after all




"Running Gun Blues"

I count the corpses on my left, I find I'm not so tidy
So I better get away, better make it today
I've cut twenty-three down since Friday
But I can't control it, my face is drawn
My instinct still emotes it

I slash them cold, I kill them dead
I broke the gooks, I cracked their heads
I'll bomb them out from under the beds
But now I've got the running gun blues

It seems the peacefuls stopped the war
Left generals squashed and stifled
But I'll slip out again tonight
Cause they haven't taken back my rifle
For I promote oblivion
And I'll plug a few civilians

I'll slash them cold, I'll kill them dead
I'll break them gooks, I'll crack their heads
I'll slice them till they're running red
But now I've got the running gun blues





"Saviour Machine"

President Joe once had a dream
The world held his hand, gave their pledge

So he told them his scheme for a Saviour Machine

They called it the Prayer, its answer was law
Its logic stopped war, gave them food
How they adored till it cried in its boredom

'Please don't believe in me, please disagree with me

Life is too easy, a plague seems quite feasible now
or maybe a war, or I may kill you all

Don't let me stay, don't let me stay
My logic says burn so send me away

Your minds are too green, I despise all I've seen
You can't stake your lives on a Saviour Machine

I need you flying, and I'll show that dying

Is living beyond reason, sacred dimension of time
I perceive every sign, I can steal every mind

Don't let me stay, don't let me stay
My logic says burn so send me away

Your minds are too green, I despise all I've seen
You can't stake your lives on a Saviour Machine





"She Shook Me Cold"

We met upon a hill, the night was cool and still
She sucked my dormant will
Mother, she blew my brain, I will go back again
My God, she shook me cold

I had no time to spare, I grabbed her golden hair
And threw her to the ground
Father, she craved my head, Oh Lord, the things she said
My God, she should be told

I was very smart, broke the gentle hearts
Of many young virgins

I was quick on the ball, left them so lonely
They'd just give up trying

Then she took my head, smashed it up
Kept my young blood rising
Crushed me mercilessly, kept me going around

So she didn't know I crave her so-o-o

I'll give my love in vain, to reach that peak again
We met upon a hill
Mother, she blew my brain, I will go back again
My God, she shook me cold




"The Man Who Sold The World"

We passed upon the stair, we spoke of was and when
Although I wasn't there, he said I was his friend
Which came as some surprise I spoke into his eyes
I thought you died alone, a long long time ago

Oh no, not me
I never lost control
You're face to face
With The Man Who Sold The World

I laughed and shook his hand, and made my way back home
I searched for form and land, for years and years I roamed

I gazed a gazely stare at all the millions here
We must have died alone, a long long time ago

Who knows? not me
We never lost control
You're face to face
With the Man who Sold the World






"The Supermen"

When all the world was very young
And mountain magic heavy hung
The supermen would walk in file
Guardians of a loveless isle
And gloomy browed with superfear their tragic endless lives

Could heave nor sigh
In solemn, perverse serenity, wondrous beings chained to life

Strange games they would play then
No death for the perfect men
Life rolls into one for them
So softly a supergod cries

Where all were minds in uni-thought
Power weird by mystics taught
No pain, no joy, no power too great
Colossal strength to grasp a fate
Where sad-eyed mermen tossed in slumbers

Nightmare dreams no mortal mind could hold
A man would tear his brother's flesh, a chance to die
To turn to mold.

Far out in the red-sky
Far out from the sad eyes
Strange, mad celebration
So softly a supergod cries 

Far out in the red-sky
Far out from the sad eyes
Strange, mad celebration
So softly a supergod dies




