import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

import net.florito.markov.MarkovChain;


public class MarkovTest {

	// load bible
	// strip 1Sa 2:6 (word - space - word - space)

	public static void main(String[] args) {
		new MarkovTest();
	}
	
	private static String allowed = "abcdefghijklmnopqrstuvwxyz., \n"; //0123456789
	private static int[] allowedIDs = stringToInts(allowed);
	
	public MarkovTest() {
		
		// loading bible
		//String name = "bbe.txt"
		String name = "aeropagiatica pg608.txt";
		//String name = "schillerbio pg14997.txt";
		int[] bibleIDs = stringsToInts(loadText(name));
		
		
		MarkovChain markov = new MarkovChain(allowedIDs, 3);
		bibleIDs = markov.legalize(bibleIDs);
		
		
		markov.createRelationships(bibleIDs);
		
		
		
		//System.out.println(markov.relationshipsToString());
		
		
		int[] ids = markov.createChain(1500);
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<ids.length;i++) {
			sb.append((char)ids[i]);
			if ((char)ids[i]=='.') {
				sb.append("\n");
			}
		}
		System.out.println(sb.toString());
	}

	

	/**
	 * Loads bible, removes reference to chapter and makes it lower case
	 * @return
	 */
	private String[] loadText(String name) {
		
		ArrayList<String> lines = new ArrayList<String>();
		
		try {
			
			File file = null;
			try {
				File binFolder = new File(getClass().getResource("").toURI());
				file = new File(binFolder, name);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			File-  file = new File("D:\\Dropbox\\Shared Research\\java\\MarkovChain\\srctxt\\"+name);
			System.out.println("Loading "+file);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int count=0;
			while ((line=br.readLine())!=null) {
				
				int firstSpace = line.indexOf(" ");
				int secondSpace = line.indexOf(" ", firstSpace+1);
				line = line.substring(secondSpace+1);
				
				
				lines.add(line.toLowerCase()+"\n");
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return lines.toArray(new String[0]);
		
		
	}
	
	private static int[] stringToInts(String string) {
		int[] out = new int[string.length()];
		for (int i=0;i<out.length;i++) {
			out[i] = (int)string.charAt(i);
		}
		return out;
	}
	
	private static int[] stringsToInts(String[] strings) {
		ArrayList<Integer> bibleIntegers = new ArrayList<>();
		for (int i=0;i<strings.length;i++) {
			int[] lineIds = stringToInts(strings[i]);
			for (int j=0;j<lineIds.length;j++) {
				bibleIntegers.add(lineIds[j]);
			}
		}
		int[] bibleIDs = new int[bibleIntegers.size()];
		for (int i=0;i<bibleIDs.length;i++) {
			bibleIDs[i] = bibleIntegers.get(i);
		}
		return bibleIDs;
	}
	
}
