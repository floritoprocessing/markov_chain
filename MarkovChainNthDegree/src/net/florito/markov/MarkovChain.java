package net.florito.markov;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarkovChain {

	public int degree;
	private int[] allowedIDs;
	
	private Relationships[] relationships;
	
	/**
	 * Markov chain with one degree less
	 */
	private MarkovChain subMarkovChain = null;
	
	
	/**
	 * General stack distribution of IDs
	 * i.e. 'a' -> 0..0.1, 'b' -> 0.1..0.12, etc..
	 */
	private float[] idStackedPercentage; //idPercLow, 
	private boolean isMasterMarkovChain = false;
	private boolean stackCreated = false;
	
	public MarkovChain(int[] allowedIDs, int degree) {
		this(allowedIDs, degree, null);
		isMasterMarkovChain = true;
	}
	
	private MarkovChain(int[] allowedIDs, int degree, float[] idPercHigh) { //float[] idPercLow, 
		
		System.out.println("MarkovChain("+degree+")");
		
		// degree of markov
		this.degree = Math.max(1, degree);
		
		// setup allowedIDs
		this.allowedIDs = new int[allowedIDs.length];
		System.arraycopy(allowedIDs, 0, this.allowedIDs, 0, allowedIDs.length);
		//System.out.println(allowedIDs.length);
		
		if (idPercHigh==null) {
			//this.idPercLow = new float[allowedIDs.length];
			this.idStackedPercentage = new float[allowedIDs.length];
		} else {
			//this.idPercLow = idPercLow;
			this.idStackedPercentage = idPercHigh;
		}
		
		
		/*
		 * I want to get i.e.
		 * id = [a,a] .. [z,z]
		 * so
		 * id = [0,0] .. [25,25]
		 * 
		 * allowedIDslength = 25;
		 * allCombinationAmount = 25*25;
		 * in loop i 0..allowedIDsLength {
		 *   id[0] = a, b, c, d, e ... etc.. z, then a, b, c, d, e, ..
		 *   id[1] = a, a, a, a, a ... etc.. a, then b, b, b, b, b, ..
		 * }
		 * so,
		 * digit0: index = i%allowedIDslength
		 * digit1: index = (i/allowedIDslength)%allowedIDslength
		 * digit2: index = (i/all*all)%allowedIDslength
		 * so
		 * id[index] = allowedIDs[index]
		 */
		
		int allowedIDsLength = allowedIDs.length;
		int allCombinationAmount = (int)(Math.pow(allowedIDs.length,degree));
		relationships = new Relationships[allCombinationAmount];
		int[] idCombination = new int[degree]; // i.e. 'DA' (degree=2)
		for (int i=0;i<allCombinationAmount;i++) {
			for (int digit=0;digit<degree;digit++) {
				int index = (i/(int)(Math.pow(allowedIDsLength, digit))) % allowedIDsLength;
				idCombination[digit] = allowedIDs[index];
				//char c = (char)idCombination[digit];
				///System.out.print(c=='\n'?"[NL]":c);
				//if (digit<degree-1) System.out.print("-");
			}
			//System.out.println();
			System.out.println("\tinitializing relationship "+i+"/"+allCombinationAmount);
			relationships[i] = new Relationships(allowedIDs, 
					idCombination);
		}
		
		// now that we are done, let's create the markov chain for one lower degrees.
		if (degree>1) {
			subMarkovChain = new MarkovChain(allowedIDs, degree-1, idPercHigh);
		}
	}
	

	/**
	 * Removes non-allowed IDs
	 * @param ids
	 * @return
	 */
	public int[] legalize(int[] ids) {
		ArrayList<Integer> result = new ArrayList();
		for (int i=0;i<ids.length;i++) {
			if (isLegalID(ids[i])) {
				result.add(ids[i]);
			}
		}
		int[] out = new int[result.size()];
		for (int i=0;i<out.length;i++) {
			out[i] = result.get(i);
		}
		return out;
	}

	private boolean isLegalID(int id) {
		for (int i=0;i<allowedIDs.length;i++) {
			if (id==allowedIDs[i]) {
				return true; 
			}
		}
		return false;
	}

	

	
	
	
	public void createRelationships(int[] IDs) {
		
		if (subMarkovChain!=null) {
			subMarkovChain.createRelationships(IDs);
		}
		
		System.out.println("createRelationships(); degree="+degree);
		float lastShownPercentage = -1000;
		for (int i=0;i<IDs.length-degree-1;i++) {
			
			int[] sourceIDs = new int[degree];
			for (int j=0;j<degree;j++) {
				sourceIDs[j] = IDs[i+j];
			}
			
			int targetID = IDs[i+degree];
			
			countRelationships(targetID, sourceIDs);
			
			float percentage = 100f*i/IDs.length;
			if (percentage==0) {
				System.out.print("Loading.");
			}
			if (Math.abs(percentage-lastShownPercentage)>=5) {
				//System.out.printf("%nLoading (%d)",(int)percentage);
				System.out.printf("%d...",(int)percentage);
				lastShownPercentage = percentage;
			}
			if (i==IDs.length-degree-2) {
				System.out.println();
			}
			
		}
		
		createStacks(IDs);
	}

	
	/**
	 * @param targetID  i.e. 'o'
	 * @param sourceIDs i.e. ['d','e','m']
	 */
	private void countRelationships(int targetID, int... sourceIDs) {
		for (int i=0;i<relationships.length;i++) {
			if (Arrays.equals(relationships[i].sourceIDCombination, sourceIDs)) {
				relationships[i].addOneTo(targetID);
				break;
			}
		}
	}
	
	private void createStacks(int[] IDs) {
		
		System.out.println("createStacks(); degree="+degree);
		
		if (isMasterMarkovChain) {
			// create idPercLow, idPercHigh -> the sub ones are done by reference
			
			int totalCount = 0; // was long
			int[] countPerId = new int[allowedIDs.length]; // was long
			
			for (int i=0;i<IDs.length;i++) {
				int id = IDs[i];
				int idIndex = -1;
				for (int j=0;j<allowedIDs.length;j++) {
					if (allowedIDs[j]==id) idIndex = j;
				}
				//System.out.println("Id="+id+"("+(char)id+") -> idIndex="+idIndex);
				countPerId[idIndex]++;
				totalCount++;
			}
			
			System.out.println("letter distrib");
			for (int i=0;i<allowedIDs.length;i++) {
				float idPerc = (float)((double)countPerId[i] / totalCount);
				System.out.println(allowedIDs[i]+" "+(char)allowedIDs[i]+": "+idPerc*100);
				if (i==0) {
					//idPercLow[i] = 0;
					idStackedPercentage[i] = idPerc;
				} else {
					//idPercLow[i] = idPercHigh[i-1];
					idStackedPercentage[i] = idStackedPercentage[i-1] + idPerc;
				}
			}
			
		}
		
		for (Relationships r:relationships) {
			r.createCountStack();
		}
		
		stackCreated = true;
	}
	
	
	
	

	public int[] createChain(int count) {
		if (!stackCreated) {
			throw new RuntimeException("Stack not yet created");
		}
		
		int[] out = new int[count];
		
		// start with first letter
		float random = (float)Math.random();
		int foundIndex = -1;
		
		System.out.print("idPercentages: ");
		for (float idph:idStackedPercentage) System.out.println(idph+" ");
		System.out.println();
		
		for (int i=0;i<allowedIDs.length;i++) {
			if (random<=idStackedPercentage[i]) {
				foundIndex = i;
				break;
			}
		}
		if (foundIndex==-1) {
			throw new RuntimeException("Something wrong...");
		} else {
			out[0] = allowedIDs[foundIndex];
			System.out.println("First letter = "+out[0]+" "+(char)out[0]);
		}
		
		// all other letters.
		for (int i=1;i<out.length;i++) {
			random = (float)Math.random();
			
			// with i.e. degree = 2:
			// i.e. out = 'h' -> sourceIDs = 'h'
			// i.e. out = 'he' -> sourceIDs = 'he'
			// i.e. out = 'hel' -> sourceIDs = 'el'
			
			int[] sourceIDs = new int[Math.min(i, degree)];
			for (int j=0;j<sourceIDs.length;j++) {
				int revJ = sourceIDs.length-1-j; // 0,1 -> 1,0
				//System.out.println("Setting sourceIDs["+revJ+"] to out["+(i-j)+"], which is "+out[i-j]);
				sourceIDs[revJ] = out[i-j-1];
			}
			
			/*System.out.println("sourceIDS: "+intArrayToString(sourceIDs)+" ("+
					intArrayToChars(sourceIDs)+")");*/
			
			out[i] = getNextId(sourceIDs, random);
		}
		
		
		// TODO Auto-generated method stub
		return out;
	}
	
	
	public int getNextId(int[] sourceIDs, float random) {
		if (sourceIDs.length<degree) {
			return subMarkovChain.getNextId(sourceIDs, random);
		} else {
			
			// found i.e. 'ba'
			for (Relationships r:relationships) {
				if (Arrays.equals(sourceIDs, r.sourceIDCombination)) {
					return r.getTargetID(random);
				}
			}
			
			StringBuilder sbInts = new StringBuilder();
			for (int id:sourceIDs) sbInts.append(id+" ");
			StringBuilder sbChars = new StringBuilder();
			for (int id:sourceIDs) sbChars.append((char)id+" ");
			throw new RuntimeException("Weird, could not generate next id from "+sbInts.toString()+" ("+sbChars.toString()+")");
		}
	}
	

	public String relationshipsToString() {
		StringBuilder sb = new StringBuilder();
		for (Relationships r:relationships) {
			sb.append(r.toString(true));
			sb.append("-------------------\n");
		}
		return sb.toString();
	}


	
	
	
	
	
	
	public static String intArrayToString(int[] arr) {
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<arr.length;i++) {
			sb.append(arr[i]);
			if (i<arr.length-1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}
	
	public static String intArrayToChars(int[] arr) {
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<arr.length;i++) {
			sb.append((char)arr[i]);
			if (i<arr.length-1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}
}
