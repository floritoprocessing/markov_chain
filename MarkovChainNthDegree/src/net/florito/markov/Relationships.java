package net.florito.markov;

import java.util.Arrays;

public class Relationships {

	// i.e. 'de'
	public final int[] sourceIDCombination;
	
	// i.e. 'a', 'b', 'c', ...
	private final int[] destinationIDs;
	// i.e. 13, 1007, 587, ... 
	private int[] relationshipCount; // was long
	
	
	
	
	public int[] getRelationshipCount() {
		return relationshipCount;
	}

	private boolean stackFinalized = false;
	
	/**
	 * i.e. 0..1000, 1000..1080, 1080..5122, etc..
	 */
	//private long[] countLow, countHigh;
	private float[] percHigh; //percLow, 
	
	
	
	public Relationships(int[] allowedIDs, int... sourceID) {
		this.sourceIDCombination = new int[sourceID.length];
		System.arraycopy(sourceID, 0, this.sourceIDCombination, 0, sourceID.length);
		
		this.relationshipCount = new int[allowedIDs.length];
		
		this.destinationIDs = new int[allowedIDs.length];
		System.arraycopy(allowedIDs, 0, destinationIDs, 0, allowedIDs.length);
		
	}
	
	public void addOneTo(int destinationID) {
		int foundIndex = -1;
		for (int i=0;i<destinationIDs.length;i++) {
			if (destinationID==destinationIDs[i]) {
				foundIndex = i;
				break;
			}
		}
		if (foundIndex != -1) {
			relationshipCount[foundIndex]++;
		}
	}
	
	public void createCountStack() {
		//long[] countLow = new long[relationshipCount.length];
		long[] countHigh = new long[relationshipCount.length];
		//percLow = new float[relationshipCount.length];
		percHigh = new float[relationshipCount.length];
		long totalCount = 0;
		for (int i=0;i<relationshipCount.length;i++) {
			totalCount += relationshipCount[i];
			if (i==0) {
				//countLow[i] = 0;
				countHigh[i] = relationshipCount[i];
			}
			else {
				//countLow[i] = countHigh[i-1];
				countHigh[i] = countHigh[i-1] + relationshipCount[i];
			}
		}
		
		for (int i=0;i<relationshipCount.length;i++) {
			//percLow[i] = (float)((double)countLow[i]/totalCount);
			percHigh[i] = (float)((double)countHigh[i]/totalCount);
		}
		
		
		stackFinalized = true;
	}
	
	
	public int getTargetID(float random) {
		if (!stackFinalized) {
			throw new RuntimeException("Sorry, stack not finalized. Use createCountStack()");
		}
		int foundIndex = -1;
		for (int i=0;i<relationshipCount.length;i++) {
			
			if (random<percHigh[i]) {
				foundIndex = i;
				break;
			}
			
			/*if ( (i<relationshipCount.length-1 && percLow[i]>=random && percHigh[i]<random) ||
				 (i==relationshipCount.length-1 && percLow[i]>=random && percHigh[i]<=random)
					) {
				
				foundIndex = i;
				break;
				
			}*/
		}
		if (foundIndex==-1) {
			throw new RuntimeException("Something must have gone wrong stacking..?");
		} else {
			return destinationIDs[foundIndex];
		}
	}
	
	
	
	public String toString(boolean onlyNonZeroRelationships) {
		String name = "'";
		for (int i=0;i<sourceIDCombination.length;i++) {
			String c = ""+(char)sourceIDCombination[i];
			if (c.equals("\n")) {
				c = "[NL]";
			}
			name += c;// + (i<sourceIDCombination.length-1?"-":"");
		}
		name += "'";
		
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("\n");
		
		for (int i=0;i<destinationIDs.length;i++) {
			if (!onlyNonZeroRelationships || relationshipCount[i]!=0) {
				String c = ""+(char)destinationIDs[i];
				if (c.equals("\n")) {
					c = "[NL]";
				}
				sb.append("\t" + c + " ("+relationshipCount[i]+")");
				sb.append("\n");
			}
		}
		
		return sb.toString();
	}
	
	/*
	 * Equality based only on sourceIDCombination
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Relationships other = (Relationships) obj;
		if (!Arrays.equals(sourceIDCombination, other.sourceIDCombination))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(sourceIDCombination);
		return result;
	}

	
}
