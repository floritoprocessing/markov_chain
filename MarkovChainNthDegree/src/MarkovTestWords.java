import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

import net.florito.markov.MarkovChain;


public class MarkovTestWords {

	// load bible
	// strip 1Sa 2:6 (word - space - word - space)

	public static void main(String[] args) {
		new MarkovTestWords();
	}
	
	//private static String allowed = "abcdefghijklmnopqrstuvwxyz., \n"; //0123456789
	//private static int[] allowedIDs = stringToInts(allowed);
	
	public MarkovTestWords() {
		
		// loading bible
		String name = "bbe.txt";
		//String name = "aeropagiatica pg608.txt";
		//String name = "schillerbio pg14997.txt";
		String bibleLines[] = loadText(name);
		String[] allWordsIncludingDotsCommasAndReturns = 
				createSeparatedDotsCommasAndReturns(bibleLines);
		
		
		// get all unique words
		ArrayList<String> uniqueWords = new ArrayList<>();
		int count=0;
		for (String l:allWordsIncludingDotsCommasAndReturns) {
			if (!uniqueWords.contains(l)) {
				uniqueWords.add(l);
			}
			if (count%20000==0) {
				System.out.println(100f*(float)count/(float)allWordsIncludingDotsCommasAndReturns.length);
			}
			count++;
		}
		System.out.println(uniqueWords.size()+" unique words");
		
		// generate all allowed IDs (i.e. he=0, but=1, then=2, etc..)
		int[] allowedIDs = new int[uniqueWords.size()];
		for (int i=0;i<allowedIDs.length;i++) {
			allowedIDs[i]  = i;
		}
		
		// the bible in words
		int[] bibleIDs = new int[allWordsIncludingDotsCommasAndReturns.length];
		for (int i=0;i<allWordsIncludingDotsCommasAndReturns.length;i++) {
			int id = uniqueWords.indexOf(allWordsIncludingDotsCommasAndReturns[i]);
			bibleIDs[i] = id;
		}
		
		MarkovChain markov = new MarkovChain(allowedIDs, 1);
		bibleIDs = markov.legalize(bibleIDs);
		markov.createRelationships(bibleIDs);
		
		
		int[] ids = markov.createChain(500);
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<ids.length;i++) {
			sb.append(uniqueWords.get(ids[i])+" ");
		}
		System.out.println(sb.toString());
		
	}

	

	private String[] createSeparatedDotsCommasAndReturns(String[] bibleLines) {
		
		ArrayList<String> out = new ArrayList<>();
		for (String bibleLine:bibleLines) {
			
			bibleLine = bibleLine.replaceAll("\\."," . ");
			bibleLine = bibleLine.replaceAll("\\?"," ? ");
			bibleLine = bibleLine.replaceAll(","," , ");
			bibleLine = bibleLine.replaceAll(":"," : ");
			bibleLine = bibleLine.replaceAll(";"," ; ");
			bibleLine = bibleLine.replaceAll("\\("," ( ");
			bibleLine = bibleLine.replaceAll("\\)"," ) ");
			bibleLine = bibleLine.replaceAll("\n"," \n ");
			bibleLine = bibleLine.trim().replaceAll(" +"," ");
			
			String[] bibleWords = bibleLine.split(" ");
			out.addAll(Arrays.asList(bibleWords));
			out.add("\n");
		}
		
		return out.toArray(new String[0]);
	}



	/**
	 * Loads bible, removes reference to chapter and makes it lower case
	 * @return
	 */
	private String[] loadText(String name) {
		
		ArrayList<String> lines = new ArrayList<String>();
		
		try {
//			File file = new File("D:\\Dropbox\\Shared Research\\java\\MarkovChain\\srctxt\\"+name);
			File file = null;
			try {
				File binFolder = new File(getClass().getResource("").toURI());
				file = new File(binFolder, name);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int count=0;
			while ((line=br.readLine())!=null) {
				
				int firstSpace = line.indexOf(" ");
				int secondSpace = line.indexOf(" ", firstSpace+1);
				line = line.substring(secondSpace+1);
				
				
				lines.add(line.toLowerCase()+"\n");
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return lines.toArray(new String[0]);
		
		
	}
	
	private static int[] stringToInts(String string) {
		int[] out = new int[string.length()];
		for (int i=0;i<out.length;i++) {
			out[i] = (int)string.charAt(i);
		}
		return out;
	}
	
	private static int[] stringsToInts(String[] strings) {
		ArrayList<Integer> bibleIntegers = new ArrayList<>();
		for (int i=0;i<strings.length;i++) {
			int[] lineIds = stringToInts(strings[i]);
			for (int j=0;j<lineIds.length;j++) {
				bibleIntegers.add(lineIds[j]);
			}
		}
		int[] bibleIDs = new int[bibleIntegers.size()];
		for (int i=0;i<bibleIDs.length;i++) {
			bibleIDs[i] = bibleIntegers.get(i);
		}
		return bibleIDs;
	}
	
}
