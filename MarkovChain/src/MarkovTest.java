import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import net.florito.markov.MarkovChain;


public class MarkovTest {

	// load bible
	// strip 1Sa 2:6 (word - space - word - space)

	public static void main(String[] args) {
		new MarkovTest();
	}
	
	private static String allowed = " abcdefghijklmnopqrstuvwxyz.,"; //0123456789
	
	public MarkovTest() {
		
		
		
//		try {
//			File thisFolder = new File(getClass().getResource("").toURI().toURL().getPath());
//			File projectFolder = thisFolder.getParentFile().getParentFile();
//			File sourceTextFolder = 
//			System.out.println(projectFolder);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (true) return;
		
		// loading bible
		String[] bible = loadBible();
		System.out.println(bible.length);
		
		MarkovChain markov = new MarkovChain();
		markov.clearTraining();
		
		
		for (String line:bible) {
			int lastC=-1;			
			for (int i=0;i<line.length()-1;i++) {
				
				char char0 = line.charAt(i);
				char char1 = line.charAt(i+1);
				if (allowed.indexOf(char0)>=0 && allowed.indexOf(char1)>=0) {
					int c0 = (int)char0;
					int c1 = (int)char1;
					lastC = c1;
					markov.train(c0,c1);
				}
				
			}
			if (lastC!=-1) {
				markov.train(lastC,(int)'\n');
			}
		}
		
		markov.finalizeTraining();
		
		int[] ids = markov.createChain(500);
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<ids.length;i++) {
			sb.append((char)ids[i]);
		}
		System.out.println(sb.toString());
	}

	/**
	 * Loads bible, removes reference to chapter and makes it lower case
	 * @return
	 */
	private String[] loadBible() {
		
		ArrayList<String> lines = new ArrayList<String>();
		
		BufferedReader br=null;
		try {
			
			ClassLoader classLoader = getClass().getClassLoader();
			InputStream in = classLoader.getResourceAsStream("bbe.txt");
//			File file = new File(classLoader.getResource("bbe.txt").getFile());
			
			
//			File file = null;
//			try {
//				file = new File(getClass().getResource("./bbe.txt").toURI().toURL().getPath());
//			} catch (Exception e) {
//				e.printStackTrace();
//				System.exit(-1);
//			}
			
//			File file = new File("bbe.txt");
//			br = new BufferedReader(new FileReader(file));
			br = new BufferedReader(new InputStreamReader(in));
			String line;
//			int count=0;
			while ((line=br.readLine())!=null) {
				
				int firstSpace = line.indexOf(" ");
				int secondSpace = line.indexOf(" ", firstSpace+1);
				line = line.substring(secondSpace+1);
				
				lines.add(line.toLowerCase());
			}
			br.close();
			br = null;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return lines.toArray(new String[0]);
		
		
	}
	
	
}
