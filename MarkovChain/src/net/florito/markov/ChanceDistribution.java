package net.florito.markov;

public class ChanceDistribution {

	public float chanceOrCount;
	public int targetId;
	
	public ChanceDistribution(int targetId, float chanceOrCount) {
		this.chanceOrCount = chanceOrCount;
		this.targetId = targetId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + targetId;
		return result;
	}

	/**
	 * Equality based only on targetStateId
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChanceDistribution other = (ChanceDistribution) obj;
		if (targetId != other.targetId)
			return false;
		return true;
	}
	
}
