package net.florito.markov;

public class Range {

	public final int id;
	public final float low;
	public final float high;
	
	public Range(int id, float low, float high) {
		this.id = id;
		this.low = low;
		this.high = high;
	}
	
}
