package net.florito.markov;

import java.util.ArrayList;

/**
 * A list of ChanceDistribution objects (targetID, chance)
 * @author Marcus
 *
 */
public class MarkovProperty {
	
	
	private final ArrayList<ChanceDistribution> relationships = new ArrayList<ChanceDistribution>();
	private final ArrayList<Range> ranges = new ArrayList<Range>();
	private boolean finalized = false;
	
	public MarkovProperty() {
	}
	
	public MarkovProperty(int targetStateId, float chanceOrCount) {
		ChanceDistribution cd = new ChanceDistribution(targetStateId, chanceOrCount);
		relationships.add(cd);
	}
	
	public void clear() {
		relationships.clear();
		ranges.clear();
		finalized = false;
	}
	
	public ChanceDistribution[] getRelationships() {
		return relationships.toArray(new ChanceDistribution[0]);
	}
	
	public float getChance(int targetStateId) {
		ChanceDistribution cd = new ChanceDistribution(targetStateId, 0);
		int index = relationships.indexOf(cd);
		if (index==-1) {
			return 0;
		} else {
			return relationships.get(index).chanceOrCount;
		}
	}
	
	/**
	 * Adds or overwrites the chance for an existing targetStateId
	 * @param targetId
	 * @param chanceOrCount
	 */
	public void add(int targetId, float chanceOrCount) {
		ChanceDistribution cd = new ChanceDistribution(targetId, chanceOrCount);
		int index = relationships.indexOf(cd);
		
		if (index==-1) {
			relationships.add(cd);
		} else {
			relationships.get(index).chanceOrCount = chanceOrCount;
		}
	}
	
	/**
	 * Generates a stack of chances<br>
	 * Let's say we have:
	 * <ul>
	 * <li> 'a' 0.7%
	 * <li> 'b' 0.1%
	 * <li> 'c' 0.2%
	 * </ul>
	 * This will generate a list of ranges:
	 * <ul>
	 * <li> 'a' 0.0 .. 0.7
	 * <li> 'b' 0.7 .. 0.8
	 * <li> 'c' 0.8 .. 1.0
	 * </ul>
	 */
	public void finalize() {
		
		float low=0;
		float high=0;
		ranges.clear();
		
		boolean first = true;
		for (ChanceDistribution cd:relationships) {
			if (first) {
				first = false;
				high = cd.chanceOrCount;
			} else {
				low = high;
				high = low + cd.chanceOrCount;
			}
			
			ranges.add(new Range(cd.targetId, low, high));
		}
		
		finalized = true;
	}

	public int getIdWithChance(float random) {
		if (!finalized) {
			throw new RuntimeException("You can't use getIdWithChance when you haven't called finalize first!");
		}
		
		for (Range r:ranges) {
			if (random>=r.low && random<r.high) {
				return r.id;
			}
		}
		
		System.err.println("Somthing gone wrong on ranging..");
		System.err.println("Value was "+random);
		for (Range r:ranges) {
			System.err.printf("%d: %.2f %.2f%n", r.id, r.low, r.high);
		}
		throw new RuntimeException("Somthing gone wrong on ranging..");
	}

}
