package net.florito.markov;

import java.util.Enumeration;
import java.util.Hashtable;

public class MarkovChain {
	
	
	private final MarkovProperty distribution = new MarkovProperty();
	
	/**
	 * ID -> List of ChanceDistribution (chance, ID)
	 */
	private final Hashtable<Integer, MarkovProperty> markovProperties = new Hashtable<Integer, MarkovProperty>();
	
	/**
	 * ID -> List of ChanceDistribution (chance, ID)
	 * Using chance as count for stats
	 */
	private final Hashtable<Integer, MarkovProperty> markovStats = new Hashtable<Integer, MarkovProperty>();
	
	
	
	/*
	 * 
	 * TRAINING
	 * 
	 */
	
	public void clearTraining() {
		markovStats.clear();
	}
	
	public void train(int sourceId, int targetId) {
		Integer sourceKey = new Integer(sourceId);
		
		MarkovProperty mp = markovStats.get(sourceKey);
		// no source key yet
		if (mp == null) {
			mp = new MarkovProperty(targetId, 1);
			markovStats.put(sourceKey, mp);
		}
		
		// yes key
		else {
			int count = (int)mp.getChance(targetId);
			mp.add(targetId, count+1);
		}
		
	}
	
	
	
	public void finalizeTraining() {
		
		markovProperties.clear();
		
		int totalRelationships = 0;
		for (Enumeration<Integer> keys = markovStats.keys(); keys.hasMoreElements();) {
			
			// get source key (i.e. 'a')
			Integer sourceKey = keys.nextElement();
			System.out.print("SOURCE "+(char)sourceKey.intValue());
			
			MarkovProperty mp = markovStats.get(sourceKey);
			ChanceDistribution[] relationships = mp.getRelationships();
			
			// add total relationships to calc chance per key
			totalRelationships += relationships.length;
			
			// now go through all relationships and count the amount of connections
			int totalCount = 0;
			for (int i=0;i<relationships.length;i++) {
				totalCount += (int)relationships[i].chanceOrCount;
			}
			System.out.println(" ("+totalCount+" relationships):");
			
			// now go through all of them again to calculate the chances
			float totalChance = 0;
			for (int i=0;i<relationships.length;i++) {
				int targetId = relationships[i].targetId;
				int count = (int)relationships[i].chanceOrCount;
				float chance = (float)count / (float)totalCount;
				totalChance += chance;
				addState(sourceKey, targetId, chance);
				String c = ""+(char)targetId;
				if (targetId==(int)'\n') {
					c = "[NL]";
				}
				System.out.printf("\t '"+c+"' %.1f %n", chance*100);
			}
			System.out.printf("\t total %.1f %n", totalChance*100);
			
			//mp.finalize();
		}
		
		for (Enumeration<Integer> keys = markovProperties.keys(); keys.hasMoreElements();) {
			markovProperties.get(keys.nextElement()).finalize();
		}
		
		
		
		System.out.println("SOURCE KEYS:");
		distribution.clear();
		for (Enumeration<Integer> keys = markovStats.keys(); keys.hasMoreElements();) {
			Integer sourceKey = keys.nextElement();
			int relationshipCount = markovStats.get(sourceKey).getRelationships().length;
			float chance = (float)relationshipCount / (float)totalRelationships;
			distribution.add(sourceKey, chance);
			String c = ""+(char)sourceKey.intValue();
			if (sourceKey.intValue()==(int)'\n') {
				c = "[NL]";
			}
			System.out.printf("SOURCE "+c+" %.1f%n", chance*100);
		}
		distribution.finalize();
		
		
	}
	
	
	
	
	/*
	 * 
	 * Markov states
	 * 
	 */
	
	
	
	public void addState(int sourceId, int targetId, float chance) {
		
		Integer id = new Integer(sourceId);
		
		// source id does not yet exist
		if (!markovProperties.containsKey(id)) {
			markovProperties.put(id, new MarkovProperty(targetId, chance));
		}
		
		// source id Does exist
		else {
			MarkovProperty markovProperty = markovProperties.get(id);
			markovProperty.add(targetId, chance);
		}
		
	}

	
	
	/*
	 * 
	 * Generate Chain
	 * 
	 */
	

	private int[] createChain(int firstId, int count) {
		
		int[] out = new int[count];
		out[0] = firstId;
		
		int lastId = firstId;
		for (int i=1;i<count;i++) {
			System.out.println("Getting markovProp for "+(char)lastId);
			MarkovProperty mp = markovProperties.get(lastId);
			int nextId = mp.getIdWithChance((float)Math.random());
			//System.out.println((char)nextId);
			
			for (int j=0;j<i;j++) {
				System.out.print((char)out[j]);
			}
			System.out.println();
			
			out[i] = nextId;
			lastId = nextId;
		}
		
		return out;
	}
	
	public int[] createChain(int count) {
		int firstId = distribution.getIdWithChance((float)Math.random());
		System.out.println((char)firstId);
		return createChain(firstId, count-1);
	}


	

	
	
}
