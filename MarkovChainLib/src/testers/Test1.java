package testers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

import net.florito.markovchain.MarkovChain;

public class Test1 {

	
	
	
	public Test1() {
		String filename = "bbeNoChaptersUntilRut.txt";
		
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream in = classLoader.getResourceAsStream(filename);
		
		//String filename = "aeropagiatica pg608 no empty lines.txt";
//		File path = new File("D:\\Dropbox\\Shared Research\\java\\MarkovChain source texts");
//		File file = new File(path , filename);
		
		String text = loadText(in, false);
		String modText = separateSpecialChars(text).replaceAll(" +"," ");
		String[] words = modText.split(" ");
		int max = 10000;
		if (words.length>max) {
			String[] temp = new String[max];
			System.arraycopy(words, 0, temp, 0, max);
			words = temp;
		}
		
		MarkovChain<String> chain = new MarkovChain<>(4);
		chain.train(words);
		
		
		/*ArrayList<ChainObject<String>>regWords = chain.getChainObjects();
		for (ChainObject<String> regWord:regWords) {
			//if (regWord.count>1) {
				System.out.println(regWord);
			//}
		}*/
		System.out.println(chain.getChainObjectAmount()+" different combinations out of "+words.length+" objects.");
		
		for (int l=0;l<10;l++) {
			ArrayList<String> objects = chain.createChainUntil(".");
			for (int i=0;i<objects.size();i++) {
				System.out.print(objects.get(i)+" ");
			}
			System.out.println();
		}
	}

	
	
	
	private static String newLine = "NEWLINE";//System.getProperty("line.separator");
	static private String loadText(InputStream in, boolean newlineIsObject) {
		System.out.print("Loading "+in.toString()+"... ");
		String text;
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line=reader.readLine())!=null) {
				sb.append(line.toLowerCase());
				if (newlineIsObject) {
					sb.append(" "+newLine+" ");
				} else {
					sb.append(" ");
				}
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		text = sb.toString();
		System.out.println("done.");
		return text;
	}
	
	
	static char[] specialSigns = new char[] { '(', ')', '.', ':', ';', ',', '?', '!' };
	static boolean isSpecialSign(char c) {
	  for (int i=0;i<specialSigns.length;i++) {
	    if (c==specialSigns[i]) {
	      return true;
	    }
	  }
	  return false;
	}
	
	static private String separateSpecialChars(String text) {
		System.out.print("Separating characters... ");
		
		//Pattern pattern = Pattern.compile("[A-Za-z0-9]");
		Pattern pattern = Pattern.compile("[^\\x0B\\f]");
		//TODO Remove weird chars
		
		StringBuilder sb = new StringBuilder();
		int textLen = text.length();
		char c;
		for (int i=0;i<textLen;i++) {
			c = text.charAt(i);
			if (isSpecialSign(c)) {
				sb.append(" "+c+" ");
			} else if (pattern.matcher(""+c).find()){
				sb.append(c);
			}
		}
		System.out.println("done.");
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		new Test1();
	}
}
