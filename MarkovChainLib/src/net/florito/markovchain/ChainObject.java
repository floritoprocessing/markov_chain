package net.florito.markovchain;

import java.util.Arrays;

/**
 * I.e. "a" or "F#"
 * @author Marcus
 *
 * @param <T>
 */
public class ChainObject<T> {

	static long nextID = 0;
	
	public final long id;
	public final T[] objects;
	private final String idObjectString;
	
	public long count = 1;
	public long stackedCount = -1;
	
	public ChainObject(T[] objects) {
		this.id = nextID++;
		this.objects = objects;
		idObjectString = "ChainObject ("+arrToString(objects)+", id="+id;
	}
	
	public ChainObject(T object) {
		T[] objects = (T[])new Object[] {object};
		this.id = nextID++;
		this.objects = objects;
		idObjectString = "ChainObject ("+arrToString(objects)+", id="+id;
	}
	
	public int getObjectCount() {
		return objects.length;
	}
	
	public String toString() {
		return idObjectString+", count="+count+", stackedCount="+stackedCount+")";
	}

	public String arrToString(T[] arr) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i=0;i<arr.length;i++) {
			sb.append(arr[i].toString());
			if (i<arr.length-1)
				sb.append("|");
		}
		sb.append("]");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(objects);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChainObject other = (ChainObject) obj;
		if (!Arrays.equals(objects, other.objects))
			return false;
		return true;
	}
	
	
	
	
}
