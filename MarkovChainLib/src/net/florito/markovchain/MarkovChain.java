package net.florito.markovchain;

import java.util.ArrayList;

public class MarkovChain<T> {
	
	public static boolean DEBUG_TRAINING = true;
	public static boolean DEBUG_FINDING = false;

	private ArrayList<ChainObject<T>> chainObjects = new ArrayList<>();
	private final int degree;
	private long stackedCount = -1;
	
	public MarkovChain(int degree) {
		this.degree = Math.max(1, degree);
	}
	
	@SuppressWarnings("unchecked")
	public void train(T[] objects) {
		if (DEBUG_TRAINING) {
			System.out.println("train()");
			System.out.print("  Registering..\t");
		}
		
		// clear all objects
		clearChainObjects();
		
		int amount = objects.length;
		PercentageDisplay percentageDisplay = new PercentageDisplay(0.05f);
		for (int i=0;i<amount;i++) {
			
			//percentage display
			if (DEBUG_TRAINING) {
				percentageDisplay.update((float)i/(float)(amount-1));
			}
			
			// 1st degree = [i]-[i+1] (minimum!)
			// register [i], [i]-[i+1], [i]-[i+1]-[i+2], etc...
			// [i+1], [i+1]-[i+2], [i+1]-[i+2]-[i+3]
			
			ArrayList<T> chain = new ArrayList<>();
			for (int off=0;off<1+degree;off++) {
				int index = i+off;
				if (index<amount) {
					chain.add(objects[index]);
					registerChainObject((T[])chain.toArray());
				}
			}
			
		}
		
		// stack count
		stackedCount = 0;
		int objAmount = chainObjects.size();
		if (DEBUG_TRAINING) {
			System.out.print("  StackCount..\t");
		}
		percentageDisplay.clear();
		for (int i=0;i<objAmount;i++) {
			if (DEBUG_TRAINING) {
				percentageDisplay.update((float)i/(float)(objAmount-1));
			}
			ChainObject<T> obj = chainObjects.get(i);
			stackedCount += obj.count;
			obj.stackedCount = stackedCount;
		}
		
		if (DEBUG_TRAINING) {
			System.out.println("train() done.");
		}
	}
	
	public void registerChainObject(T[] object) {
		ChainObject<T> obj = new ChainObject<>(object);
		int index = chainObjects.indexOf(obj);
		if (index==-1) {
			chainObjects.add(obj);
		} else {
			chainObjects.get(index).count++;
		}
	}
	
	/*private String arrToString(ArrayList<?> arr) {
		String out ="";
		for (int i=0;i<arr.size();i++) {
			out += arr.get(i);
			if (i<arr.size()-1) 
				out+= ",";
		}
		return out;
	}*/

	public void clearChainObjects() {
		ChainObject.nextID = 0;
		chainObjects.clear();
	}
	
	public ArrayList<ChainObject<T>> getChainObjects() {
		return chainObjects;
	}

	public int getChainObjectAmount() {
		return chainObjects.size();
	}

	public ChainObject<T> getChainObject(T object) {
		ChainObject<T> obj = new ChainObject<>(object);
		int index = chainObjects.indexOf(obj);
		if (index==-1) {
			return null;
		} else {
			return chainObjects.get(index);
		}
	}

	/*
	 * 
	 * 
	 * Creation
	 * 
	 * 
	 */
	
	private ChainObject<T> randomChainObject(int objectCount) {
		ChainObject<T> out = null;
		do {
			out = null;
			double rnd = Math.random();
			long random = (long)(stackedCount*rnd);
			for (ChainObject<T> obj:chainObjects) {
				if (random<=obj.stackedCount) {
					out = obj;
					break;
				}
			}
		} while (out.getObjectCount()!=objectCount);
		if (DEBUG_FINDING) System.out.println("randomChainObject("+objectCount+") = "+out);
		return out;
	}
	
	private boolean existObjectThatFollows(ChainObject<T> previousObject) {//, int requestedChainLen) {
		for (ChainObject<T> nextObject:chainObjects) {
			
			if (isLegalFollowUp(previousObject, nextObject)) {//,	requestedChainLen)) {
				return true;
			}
			
		}
		return false;
	}
	
	private ChainObject<T> randomChainObject(ChainObject<T> previousObject, int requestedChainLen) {
		
		// build list
		/*ArrayList<ChainObject<T>> rndList = new ArrayList<>();
		for (ChainObject<T> nextObject:chainObjects) {
			if (isLegalFollowUp(previousObject, nextObject,	requestedChainLen)) {
				rndList.add(nextObject);
			}
		}*/
		
		
		ChainObject<T> out = null;
		do {
			long rnd = (long)(stackedCount * Math.random());
			for (ChainObject<T> nextObject:chainObjects) {
				if (rnd<nextObject.stackedCount) {
					if (nextObject.getObjectCount()==requestedChainLen && 
							isLegalFollowUp(previousObject, nextObject)) {
						if (DEBUG_FINDING) {
							System.out.println("randomChainObject() = " +nextObject);
						}
						out =  nextObject;
						break;
					}
				}
			}
		} while (out==null);
		
		return out;
		
		// TODO Auto-generated method stub
		//return null;
	}

	private boolean isLegalFollowUp(
			ChainObject<T> previousChain,
			ChainObject<T> nextChain) { 
			//int requestedChainLen) {
		
		T[] prevObjects = previousChain.objects;
		T[] nextObjects = nextChain.objects;
		
		// follow up is only legal,
		// if follow up is same length or 1 larger
		int prevLen = prevObjects.length;
		int nextLen = nextObjects.length;
		
		if (prevLen==nextLen || prevLen==nextLen-1) {
			
			// prevLen = nextLen-1:
			// [a][b] contained in [a][b][c];
			if (prevLen==nextLen-1) {
				for (int i=0;i<prevLen;i++) {
					if (!prevObjects[i].equals(nextObjects[i])) {
						return false;
					}
				}
				return true;
			}
			
			// prevLen = nextLen:
			// [a][b][c] vs [b][c][d];
			else {
				for (int i=0;i<prevLen-1;i++) {
					if (!prevObjects[i+1].equals(nextObjects[i])) {
						return false;
					}
				}
				return true;
			}
			
		}
		else {
			return false;
		}
		
		// true if
		// 
		
		/*//boolean legalObject = false;
		if (nextObject.objects.length!=requestedChainLen) {
			return false;
		}
		else {
			
			// "t" , requesting chainLen=2
			// "to" (compare prev[0] with obj[0])
			// "AB", requesting chainLen=3
			// "AQR", "BEX", "ABZ" (compare prev[0,1] with obj[0,1])
			// "DEF", requesting chainLen=3
			// "EFI" (compare prev[1,2] with obj[0,1])
			//        ^ index = chainLen - prevObject.length
			
			boolean isObjectOk = true;
			T[] prevObjects = previousObject.objects;
			T[] nextObjects = nextObject.objects;
			int compareLen = requestedChainLen-1; //2->1
			for (int i=0;i<compareLen;i++) {
				int prevIndex = compareLen - prevObjects.length + i;
				int objIndex = i;
				if (!prevObjects[prevIndex].equals(nextObjects[objIndex])) {
					isObjectOk = false;
				}
			}
			
			if (isObjectOk) {
				//System.out.println("Found "+previousObject.toString()+" -> "+nextObject.toString());
				//legalObject = true;//return true;
				return true;
			}
			
		}
		return false;//legalObject;
*/	}
	
	/*
	 * 
	 * 
	 * Create chain
	 * 
	 * 
	 */
	
	public ArrayList<T> createChainUntil(T endOfLine) {
		ArrayList<T> out = new ArrayList<>();
		
		ChainObject<T> link = randomChainObject(1);
		
		do {
			T last = link.objects[link.objects.length-1];
			out.add(last);
			
			if (last.equals(endOfLine)) {
				return out;
			} else {		
				int chainLen = Math.min(degree, link.objects.length+1);//out.size()+1);
				boolean chainObjectExists = existObjectThatFollows(link);//, chainLen);
				if (chainObjectExists) {
					if (DEBUG_FINDING) System.out.println("(FOUND)");
					link = randomChainObject(link, chainLen);
				} else {
					if (DEBUG_FINDING) System.out.println("(RANDOM)");
					link = randomChainObject(1);
				}
			}
		} while (true);
		
		
		/*while (!link.objects[link.objects.length-1].equals(endOfLine.objects[endOfLine.objects.length-1])) {
			
			
			
		}*/
		
		//return out;
	}

	

	/*
	 * 
	 * PercentageDisplay 
	 * 
	 */
	
	private static class PercentageDisplay {
		float lastPerc;
		float percStep;
		PercentageDisplay(float percStep) {
			this.percStep=percStep;
			clear();
		}
		void clear() {
			lastPerc = Float.MIN_VALUE;
		}
		void update(float perc) {
			if (perc==0.0) {
				lastPerc = perc;
				System.out.print(" 0..");
			}
			if (perc==1.0) {
				lastPerc = perc;
				System.out.println(100);
			} else if (Math.abs(perc-lastPerc)>=percStep) {
				lastPerc = perc;
				System.out.print((int)(perc*100)+"..");
			}
		}
	}
}
